classdef analysisTools
    % analysisTools is the automated test equipment for the RFUDCV1_0. 
    % The gm_Analysis model is required for data collection. 
    % 
    % analysisTools has public methods:
    %       - initSLRT
    %       - dataCollect
    %       - SFDRTest
    %       - SettleTimeTest
    %       - LinearityTest
    %       - attenuation_calibration
    %
    % See also initSLRT, DATACOLLECT, SFDRTEST, SETTLETIMETEST,
    % LINEARITYTEST, and ATTENUATION_CALIBRATION
    methods(Static, Access = public)
        %% Initialise target
        function initSLRT()
            % initSLRT initialises the SLRT for data recording.
            % dataCollect can be called without intialisation.
            
            dma_pause = 0.6;
            
            % Connecting to target
            fprintf("Connecting to target ... ")
            tg = slrt;

            % Configuring model parameters
            fprintf("Configuring model parameters ... ")
            tg.StopTime = inf;
            tg.SampleTime = 1e-3;
            tg.setparam('channel_select', 15); % 250 MHz
            tg.setparam('trigger_width', 3); % Optimal Trigger Pulse
            tg.setparam('gain_control', 2^14-1); % ~-2 dBm
            
            % Flush DDS index
            fprintf("Flushing DDS index... ")
            tg.start;
            tg.setparam('selection_type', 1);
            pause(dma_pause);
            tg.setparam('selection_type', 0);
            pause(dma_pause);
            tg.stop;
            
            disp("Finished")
        end
        
        %% Collect data
        function dataCollect(varargin)
            % dataCollect collects the data required to perform 3 factory
            % tests. These are the SFDRTest, SettleTimeTest, and
            % LinearityTest. dataCollect has variable input arguments and
            % no input is required
            %
            %       SFDRTest('data', data, 'filename', filename, ...)
            % 
            % dataCollect has six arguments:
            %       - boardSel: int         (default = 1)
            %       - testtype: string      (default = "All")
            %       - inputAmp: int         (default = ~ -2 dBm)
            %       - swSpeed: int          (default = 10)
            %       - description: string   (default = "None")
            %       - extra: struct         (default = extra.centre = 2)
            % 
            %       boardSel: selects the target channel for data
            %       collection. This can be Channel (1, 2, or 3).
            %
            %       testtype: determines whether the data to be collected
            %       is switching or not. Accepts either 'SettleTime' for
            %       settling time data only, 'SFDR' for SFDR data only, or
            %       'Linearity' for linearity test data only. If not
            %       specified, the data required for all three tests will
            %       be collected.
            %
            %       inputAmp: is the amplitude of the input signal in dBm.
            %       Must be less than 4dBm
            %
            %       swSpeed: is the speed at which the trigger line is
            %       switching. Useful for the settling time test.
            %
            %       description: is a string description of the different
            %       relevant test parameters
            %
            %       extra: is a struct that can contain any extra variables
            %       and is automatically saved with the file. This struct
            %       must set extra.centre in GHz.
            
            %% Variable Input Arguments
            % Check paired input
            if(mod(nargin,2))
                disp("Invalid Input Argument Pairs");
                return;
            end
            % Load variables
            for i = 1:2:nargin-1
                temp = string(varargin(i));
                switch(lower(temp))
                    case "boardsel"
                        boardSel = varargin{i+1};
                    case "testtype"
                        testtype = string(varargin{i+1});
                    case "inputamp"
                        inputAmp = abs(varargin{i+1});
                    case "swspeed"
                        swSpeed = varargin{i+1};
                    case "description"
                        description = string(varargin{i+1});
                    case "extra"
                        extra = varargin{i+1};
                    otherwise
                        fprintf("Invalid Input Argument %s", string(varargin(i)));
                        return;
                end
            end
            %% Argument Defaults
            if(~exist('boardSel', 'var'))
                boardSel = 1;
            end
            if(~exist('inputAmp', 'var'))
                inputAmp = 2^14-1;
            end
            if(~exist('swSpeed', 'var'))
                swSpeed = 10;
            end
            if(~exist('description', 'var'))
                description = "None";
            end
            if(~exist('extra.centre', 'var'))
                extra.centre = 2;
            end
            if(~exist('testtype', 'var'))
                testtype = "all";
            end
            
            %% Data Collection
            tg = slrt;
            
            init_pause = 0;
            dma_pause = 0.6;
            
            % Flush DDS index to reliably enforce DDS frequency
            fprintf("Flushing DDS index... ")
            tg.start;
            tg.setparam('selection_type', 1);
            pause(dma_pause);
            tg.setparam('selection_type', 0);
            pause(dma_pause);
            tg.stop;
            
            
            % Initialise data file
            header.boardno = string(boardSel);
            header.timestamp = string(datetime(now(),'ConvertFrom','datenum'));
            header.description = description;

            tg.setparam('board_select', boardSel);
            filename = 'Results/TestDataBrd' + string(boardSel) + '_' + string(extra.centre) + 'GHz.mat';
            save(filename, 'header');
            save(fliename, 'extra', '-append');
            
            % Only run for 'all tests' or 'static' measurement
            if(testtype ~= "SettleTime")
                % SFDR data
                fprintf("Collecting SFDR data ... ")
                tg.setparam('gain_control', inputAmp);
                tg.setparam("dma_enable", 0);
                tg.start;
                pause(init_pause);
                tg.setparam("dma_enable", 1);
                pause(dma_pause);
                tg.stop;
                dataSFDR = analysisTools.extractLogData('DMAdata.dat');
                save(filename, 'dataSFDR', '-append');
                % Exit early if only non-switching is required
                if(testtype == "SFDR")
                    return;
                end
            end
            
            % Only run for 'all tests' or 'hopping' measurement
            if(testtype ~= "SFDR" && testtype ~= "Linearity")
                % Setting time data
                fprintf("Collecting settling time data ... ")
                tg.setparam('gain_control', inputAmp);
                tg.setparam("dma_enable", 0);
                tg.setparam('selection_type', 1);
                tg.setparam('switching_period_us', swSpeed);
                tg.start;
                pause(init_pause);
                tg.setparam("dma_enable", 1);
                pause(dma_pause);
                tg.stop;
                dataSettle = analysisTools.extractLogData('DMAdata.dat');
                save(filename, 'dataSettle', '-append');
                if(testtype == "SettleTime")
                    return;
                end
            end
            
            % Output linearity data
            fprintf("Collecting amplitude linearity data ... ")
            % 3/4 Amplitude
            tg.setparam("dma_enable", 0);
            tg.setparam('selection_type', 0);
            tg.setparam('gain_control', 3*2^12-1);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            data3_4 = analysisTools.extractLogData('DMAdata.dat');
            save(filename, 'data3_4', '-append');
            
            % Half Amplitude
            tg.setparam("dma_enable", 0);
            tg.setparam('selection_type', 0);
            tg.setparam('gain_control', 2^13-1);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            dataHalf = analysisTools.extractLogData('DMAdata.dat');
            save(filename, 'dataHalf', '-append');
            
            % 3/8 Amplitude
            tg.setparam("dma_enable", 0);
            tg.setparam('selection_type', 0);
            tg.setparam('gain_control', 3*2^11-1);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            data3_8 = analysisTools.extractLogData('DMAdata.dat');
            save(filename, 'data3_8', '-append');

            % Quarter Amplitude
            tg.setparam('selection_type', 0);
            tg.setparam('gain_control', 2^12-1);
            tg.setparam("dma_enable", 0);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            dataQuarter = analysisTools.extractLogData('DMAdata.dat');
            save(filename, 'dataQuarter', '-append');

            % Eighth Amplitude
            tg.setparam('selection_type', 0);
            tg.setparam('gain_control', 2^11-1);
            tg.setparam("dma_enable", 0);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            dataEighth = analysisTools.extractLogData('DMAdata.dat');
            save(filename, 'dataEighth', '-append');

            % 1/16th Amplitude
            tg.setparam('selection_type', 0);
            tg.setparam('gain_control', 2^10-1);
            tg.setparam("dma_enable", 0);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            dataSixteenth = analysisTools.extractLogData('DMAdata.dat'); 
            save(filename, 'dataSixteenth', '-append');

            analysisTools.init()
            disp("Finished");
        end
        
        %% Spurious Free Dynamic Range (SFDR) Analysis
        function SFDRTest(varargin)
            % SFDRTest performs an SFDR calculation on raw FileScope Data.
            % The results and figures generated by the test are saved in a
            % file of specified name. SFDRTest has variable input arguments
            % but data and filename are required:
            %
            %       SFDRTest('data', data, 'filename', filename, ...)
            %
            % SFDRTest has five arguments:
            %       - data: x by 1024 double  (required)
            %       - filename: string        (required)
            %       - FFTlen: int             (default = 2^18)
            %       - sigBW: int              (default = 0)
            %       - span: int               (default = 10e6)
            %       - Fs: int                 (default = 1e9)
            %
            %       data: is the data the SFDR test is desired to be
            %       applied to. dataCollect('testtype', 'SFDR') saves the
            %       required data in the necessary format. This variable is
            %       required.
            %
            %       filename: is the filename the results and figures will
            %       be generated into. This variable is required
            %
            %       FFTlen: is the length of the FFT performed as part of
            %       the SFDR test. The default value is 2^18.
            %
            %       sigBW: is the bandwidth of the fundamental frequency.
            %       Setting this affects which peaks are counted as spurs
            %       and which are considered part of the signal. sigBW is
            %       zero by default and must be strictly less than span.
            %
            %       span: is the span of the fine SFDR window. By default
            %       this is a 10 MHz window. span must be less than 500 MHz 
            %
            %       Fs: is the sampling frequency of the data. For the
            %       Speedgoat targets this is 1 GHz. 
            
            %% Variable Input Arguments
            % Check paired input
            if(mod(nargin,2))
                disp("Invalid Input Argument Pairs");
                return;
            end
            % Load variables
            for i = 1:2:nargin-1
                temp = string(varargin{i});
                switch(lower(temp))
                    case "data"
                        dataSFDR = varargin{i+1};
                    case "filename"
                        filename = varargin{i+1};
                    case "fftlen"
                        FFTlen = varargin{i+1};
                    case "sigbw"
                        sigBW = varargin{i+1};
                    case "span"
                        span = varargin{i+1};
                    case "fs"
                        Fs = varargin{i+1};
                    otherwise
                        fprintf("Invalid Input Argument %s", string(temp));
                        return; 
                end
            end
            %% Argument Defaults
            if(~exist('dataSFDR','var') || ~exist('filename','var'))
                disp("data and filename are required inputs");
                return;
            end
            if(~exist('FFTlen','var'))
                FFTlen = 2^18;
            end
            if(~exist('sigBW','var'))
                sigBW = 0;
            end
            if(~exist('span','var'))
                span = 10e6;
            end
            if(~exist('Fs','var'))
                Fs = 1e9;
            end
            %% Run analysis
            fprintf("--------------------------------------- <strong>SFDR Analysis</strong> --------------------------------------- \n");
            [sigSFDR, ~] = analysisTools.unstack2(dataSFDR);
%             [sigSFDR, ~] = analysisTools.unstack3(dataSFDR);
            [FFT, fundamental, fine, coarse] = analysisTools.SFDR(sigSFDR, FFTlen, sigBW, span/2, Fs);
            % Save Results
            filename = strcat('Results/SFDR_', filename); 
            save(filename, 'FFT');
            save(filename, 'fundamental', '-append');
            save(filename, 'fine', '-append');
            save(filename, 'coarse', '-append');
            
            %% Plot
            FFTlen = length(FFT);
            f = figure();
            f.Position = [100 100 1000 800];
            
            % Full Span Plot
            s = 1;
            e = FFTlen;
            freq =(s:e)*fs/FFTlen/1e6/2;
            subplot(2, 2, 1:2); hold on
            if(coarse.amplitude >= -120)
                rectangle('Position',[1 coarse.amplitude 500 (fundamental.amplitude-coarse.amplitude)], 'FaceColor', '#FFEFD6', 'LineStyle', 'none');
                plot(coarse.freq/1e6, coarse.amplitude, 'v', 'Color', [0.8500 0.3250 0.0980]);
            end
            plot(fundamental.freq/1e6, fundamental.amplitude, 'v', 'Color', [0.6350 0.0780 0.1840]); hold on
            plot(freq, FFT(s:e), 'Color', 'Blue'); 
            axis([freq(1) freq(end) -120 (max(FFT)+20)])
            title('SFDR')
            xlabel('Frequency (MHz)')
            ylabel('Amplitude (dBm)')
            ratio = max(FFT)+ 80;
            text(fundamental.freq/1e6, fundamental.amplitude+0.05*ratio, "\textbf{Fundamental:}" + string(round(fundamental.amplitude,2)), 'HorizontalAlignment','center', 'Interpreter', 'latex') 
            dim = [0.69, 0.815, 0.6, 0.1];
            str = "SFDR ($<\pm$ 5 MHz): " + string(round(fine.sfdr, 2));
            annotation('textbox', 'String', str, 'Position', dim, 'FontWeight', 'bold', 'FitBoxToText', 'on','FontSize', 12, 'LineStyle', 'none', 'HorizontalAlignment','left', 'Interpreter', 'latex');
            dim = [0.69, 0.79, 0.6, 0.1];
            str = "SFDR ($>\pm$ 5 MHz): " + string(round(coarse.sfdr,2));
            annotation('textbox', 'String', str, 'Position', dim, 'FontWeight', 'bold', 'FitBoxToText', 'on', 'FontSize', 12, 'LineStyle', 'none', 'HorizontalAlignment','left', 'Interpreter', 'latex'); 
            
            % 10 MHz span Plot
            s = floor((49*FFTlen)/100);
            e = ceil((51*FFTlen)/100);
            freq =(double(s):double(e))*fs/FFTlen/1e6/2;
            subplot(2, 2, 3); hold on
            plot(freq, FFT(s:e), 'Color', 'Blue');
            if(fine.amplitude >= -120)
                plot(fine.freq/1e6, fine.amplitude, 'v', 'Color', [0.8500 0.3250 0.0980]);
                axis([freq(1) freq(end) (FFT(s)-5) (fundamental.amplitude + 5)])
            end
            title('Within 10MHz Span')
            xlabel('Frequency (MHz)')
            ylabel('Amplitude (dBm)')
            ratio = fine.amplitude+70;
            if(fine.amplitude >= -120)
                text(fine.freq/1e6, fine.amplitude+0.075*ratio, "\textbf{Spur:} \\" + string(round(fine.amplitude,2)), 'HorizontalAlignment','center', 'Interpreter', 'latex')
            end
            
            % Outside 10 MHz Plot
            s = int32(FFTlen/2 -(abs(FFTlen/2-coarse.index))*1.3); if(s < 0) s = 1; end
            e = int32(FFTlen/2 +(abs(FFTlen/2-coarse.index))*1.3); if(e > FFTlen) e = FFTlen; end
            freq =(double(s):double(e))*fs/FFTlen/1e6/2;
            subplot(2,2,4); hold on
            plot(freq, FFT(s:e), 'Color', 'Blue');
            if(coarse.amplitude >= -120)
                plot(coarse.freq/1e6, coarse.amplitude, 'v', 'Color', [0.8500 0.3250 0.0980]);
                axis([freq(1) freq(end) -120 (fundamental.amplitude + 5)])
            end
            title('Outside 10MHz Span')
            xlabel('Frequency (MHz)')
            ylabel('Amplitude (dBm)')
            if(coarse.amplitude >= -120)
                text(coarse.freq/1e6, coarse.amplitude+ 0.075*ratio, "\textbf{Spur:} \\" + string(round(coarse.amplitude,2)), 'HorizontalAlignment','center', 'Interpreter', 'latex')
            end
            
            % Display SFDR
            fprintf("<strong>SFDR(<5MHz):</strong> %2.2f\n", fine.sfdr);
            fprintf("<strong>SFDR(>5MHz):</strong> %2.2f\n", coarse.sfdr);
            disp("--------------------------------------- <strong>Complete</strong> ---------------------------------------");
        end   
        
        %% Settling Time Analysis
        function SettleTimeTest(varargin)
            % SettleTime Test performs an settling time calculation on raw
            % FileScope Data. The results and figures generated by the test
            % are saved in a file of specified name.SettleTimeTest has
            % variable input arguments but data and filename are required:
            %
            %       SettleTimeTest('data', data, 'filename', filename, ...)
            %
            % SettleTimeTest has seven arguments:
            %       - data: x by 1024 double    (required)
            %       - filename: string          (required)
            %       - FFTlen: int               (default = 2^10)
            %       - sigBW: int                (default = 0)
            %       - dec: int                  (default = 1)
            %       - swSpeed: int              (default = 10)
            %       - Fs: int                   (default = 1e9)
            %
            %       dataSettle: is the data the settling time test is
            %       desired to be applied to. dataCollect('testtype',
            %       'SettleTime') saves the required data in the necessary
            %       format. This variable is required.
            %
            %       filename: is the filename the results and figures will
            %       be generated into. This variable is required.
            %
            %       FFTlen: is the length of the FFT performed as part of
            %       the Settling Time Test. The default value is 2^10.
            %
            %       sigBW: is the bandwidth of the fundamental frequency.
            %       In the settling time test,  this is a measure of how
            %       much fluctuation in the target frequency is
            %       acceptable.. By default this is zero.
            %
            %       dec: is the decimation of the the target amplitude over
            %       time. Increasing this will smooth the target amplitude,
            %       but decrease the precision of the timesteps. By
            %       default, dec is 1 for a moving average across 2^1
            %       points.
            %
            %       swSpeed: is the switching speed of the trigger line.
            %       The default is 10 us but this variable is heavily
            %       dependent on the switching speed set in dataCollect.
            %       
            %       Fs: is the sampling frequency of the data. For the
            %       Speedgoat targets this is 1 GHz. 
            
            %% Variable Input Arguments
            % Check paired input
            if(mod(nargin,2))
                disp("Invalid Input Argument Pairs");
                return;
            end
            % Load variables
            for i = 1:2:nargin-1
                temp = string(varargin{i});
                switch(lower(temp))
                    case "data"
                        dataSettle = varargin{i+1};
                    case "filename"
                        filename = varargin{i+1};
                    case "fftlen"
                        FFTlen = varargin{i+1};
                    case "sigbw"
                        sigBW = varargin{i+1};
                    case "dec"
                        dec = varargin{i+1};
                    case "swspeed"
                        swSpeed = varargin{i+1};
                    case "fs"
                        Fs = varargin{i+1};
                    otherwise
                        fprintf("Invalid Input Argument %s", temp);
                        return;
                end
            end
            %% Argument Defaults
            if(~exist('dataSettle','var') || ~exist('filename','var'))
                disp("data and filename are required inputs");
                return;
            end
            if(~exist('FFTlen','var'))
                FFTlen = 2^10;
            end
            if(~exist('sigBW','var'))
                sigBW = 0;
            end
            if(~exist('dec','var'))
                dec = 1;
            end
            if(~exist('swSpeed','var'))
                swSpeed = 10;
            end  
            if(~exist('Fs','var'))
                Fs = 1e9;
            end
            
            %% Run analysis
            fprintf("--------------------------------------- <strong>Settling Time Analysis</strong> --------------------------------------- \n");
            [sigSettle, refSettle] = analysisTools.unstack2(dataSettle);
%             [sigSettle, refSettle] = analysisTools.unstack3(dataSettle);
            [settleTimes, slope, tgtAmp, pks, trghs, pksZero, trghsZero] = analysisTools.settleAnalysis(sigSettle, refSettle, FFTlen, sigBW, dec, swSpeed, Fs);
            filename = strcat('Results/SettlingTime_', filename); 
            save(filename, 'settleTimes');
            save(filename, 'slope', '-append');
            save(filename, 'tgtAmp', '-append');
            save(filename, 'pks', '-append');
            save(filename, 'trghs', '-append');
            save(filename, 'pksZero', '-append');
            save(filename, 'trghsZero', '-append');

            time = (1:length(tgtAmp))*(2^dec);
            timeSlope = (1:length(slope))*(2^dec);

            % Plot
            f2 = figure();
            f2.Position = [100 100 1000 800];
            
            % Amplitude over time
            y = subplot(2,1,1);
            plot(time, tgtAmp, pksZero(:,2)*(2^dec), pksZero(:,1), 'g*', trghsZero(:,2)*(2^dec), trghsZero(:,1), 'm*');
            title('Amplitude of fundamental frequency over time: 250 MHz')
            xlabel('Time (ns)')
            ylabel('Amplitude (dBm)')
            axis([timeSlope(1) timeSlope(end) floor(min(tgtAmp)) ceil(max(tgtAmp))])
            
            % Slope over time
            dy = subplot(2,1,2);
            plot(timeSlope, slope, pks(:,2)*(2^dec), pks(:,1), 'r*', trghs(:,2)*(2^dec), trghs(:,1), 'b*');
            axis([timeSlope(1) timeSlope(end) 1.2*min(slope) 1.2*max(slope)])
            title('Slope of fundamental frequency over time: 250 MHz')
            xlabel('Time (ns)')
            ylabel('Amplitude per Nanosecond (dBm/ns)')
            xlim([0,timeSlope(end)]);
            linkaxes([y, dy], 'x');
            
            % Display average settling time
            fprintf("<strong>Average settling time:</strong> %4.2f ns. \n", mean(settleTimes))
            for i = 1:length(settleTimes)
               if settleTimes(i) < 0
                   disp("Failed");
                   return; 
               end
            end
            disp("--------------------------------------- <strong>Complete</strong> ---------------------------------------");
        end
        
        %% Linearity Analysis
        function LinearityTest(dataSFDR, data3_4, dataHalf, data3_8, dataQuarter, dataEighth, dataSixteenth, filename)
            % LinearityTest performs a calculation on raw FileScope Data to
            % determine linearity between input and output. The results and
            % figures generated by the test are saved in a file of
            % specified name. Unlike the other tests Linearity only has
            % required inputs and should be called in the following format:
            %
            %       LinearityTest(dataSFDR, data3_4, dataHalf, data3_8, dataQuarter, dataEighth, dataSixteenth, filename)
            %
            % LinearityTest has eight arguments:
            %       - dataSFDR: x by 1024 double
            %       - data3_4: x by 1024 double
            %       - dataHalf: x by 1024 double
            %       - data3_8: x by 1024 double
            %       - dataQuarter: x by 1024 double
            %       - dataEighth: x by 1024 double
            %       - dataSixteenth: x by 1024 double
            %       - dataSFDR: x by 1024 double            
            %       - filename: string
            %
            %       data: are the data the linearity test is desired to be
            %       applied to. dataCollect saves the required data in the
            %       necessary format. It is only required to load the
            %       collected data into the workspace.
            %
            %       filename: is the filename the results and figures will
            %       be generated into.
            fprintf("--------------------------------------- <strong>Linearity Analysis</strong> --------------------------------------- \n");
            disp("1Vp-p");
            [sigSFDR, refSFDR] = analysisTools.unstack2(dataSFDR);
%             [sigSFDR, refSFDR] = analysisTools.unstack3(dataSFDR);
            disp("0.75Vp-p");
            [sig.one, ref.one] = analysisTools.unstack2(data3_4);
%             [sig.one, ref.one] = analysisTools.unstack3(data3_4);
            disp("0.5Vp-p");
            [sig.two, ref.two] = analysisTools.unstack2(dataHalf);
%             [sig.two, ref.two] = analysisTools.unstack3(dataHalf);
            disp("0.375Vp-p");
            [sig.three, ref.three] = analysisTools.unstack2(data3_8);
%             [sig.three, ref.three] = analysisTools.unstack3(data3_8);
            disp("0.25Vp-p");
            [sig.four, ref.four] = analysisTools.unstack2(dataQuarter);
%             [sig.four, ref.four] = analysisTools.unstack3(dataQuarter);
            disp("0.125Vp-p");
            [sig.five, ref.five] = analysisTools.unstack2(dataEighth);
%             [sig.five, ref.five] = analysisTools.unstack3(dataEighth);
            disp("0.0625Vp-p");
            [sig.six, ref.six] = analysisTools.unstack2(dataSixteenth);
%             [sig.six, ref.six] = analysisTools.unstack3(dataSixteenth);
            [io, ratio, diff, m, dev]= analysisTools.linearAnalysis(sigSFDR, refSFDR, sig, ref);
            fprintf("Max deviation from the mean is at Input Voltage %d V \n", io(dev,1));
            
            filename = strcat('Results/Linearity_', filename); 
            save(filename, 'io');
            save(filename, 'ratio', '-append');
            save(filename, 'diff', '-append');
            save(filename, 'm', '-append');
            save(filename, 'dev', '-append');
            
            figure();
            subplot(2, 1, 1);
            plot(io(:,1), io(:,2), io(:,1), m*io(:,1));
            title('Voltage Input vs Voltage Output')
            xlabel('Voltage Input (V)')
            ylabel('Voltage Output (V)')
            legend('Actual', 'Average')
            xlim([io(end,1) io(1,1)])
            
            subplot(2, 1, 2);
            plot(io(:,1), ratio); yline(m);
            title('Slope')
            xlabel('Voltage Input (V)')
            ylabel('Slope')
            legend('Actual', 'Average')
            xlim([io(end,1) io(1,1)])
            ylim([0 1])
            
        end
        
        %% Automatic Attenuation Calibration
        function pks = attenuation_calibration(varargin)
            % attenuation_calibration returns a vector of attenuation
            % settings for the RFUDC. This automated system only calculates
            % the necessary attenuation for the Rx output of each board.
            %
            % attenuation_calibration has three arguments:
            %       - channel: int      (required)
            %       - amplitude: int    (default = ~ -2 dBm)
            %       - attenuation: int  (default = 0dBm)
            %
            %       channel: is the desired output channel where the
            %       attenuation will be changed i.e., CH (1 - 3).
            %
            %       amplitude: is the gain of the input signal in dBm. The
            %       input amplitude should strictly be less than 4 dBm.
            %
            %       attenuation: is the amount of attenuation on the output
            %       before the SLRT. Attenuation is necessary to input
            %       powers higher than 4dBm into the SLRT.
            %% Variable Input Arguments
            % Check paired input
            if(mod(nargin,2))
                disp("Invalid Input Argument Pairs");
                return;
            end
            for i = 1:2:nargin-1
               temp = varargin{i};
               switch(lower(temp))
                   case "amplitude"
                       amplitude = varargin{i+1};
                   case "attenuation"
                       attenuation = varargin{i+1};
                   case "channel"
                       channel = varargin{i+1};
                   otherwise
                       fprintf("Invalid Input Argument %s", string(temp));
                       return;
               end
            end
            if(~exist('channel','var'))
                disp("channel is a required variable");
                return;
            end
            if(~exist('amplitude','var'))
                amplitude = 2^14-1;
            end
            if(~exist('attenuation','var'))
                attenuation = 0;
            end
            
            tg = slrt;
            tg.setparam('channel_select', 15); % 250 MHz
            % Convert dBm to count based gain
            amplitude = analysisTools.conversionmag(amplitude);
            tg.setparam('gain_control', amplitude); 
            tg.setparam('dma_enable', 0);
            %% Attenuation Data Collection
            for i = 2:0.2:17.8
            %    wait = input('');
               RFUDCtools.frequency_set(i,channel);
               tg.start;
            %    tg.setparam('channel_select', 4+j);
               tg.setparam('dma_enable', 1);
               pause(1);
               tg.stop;
               tg.setparam('dma_enable', 0);
               data = analysisTools.extractLogData('DMAdata.dat');
               filename = 'Attenuation_Results2/data_' + string(i) + '0dB.mat';
               save(filename, 'data');
            end
            %% Data Conversion
            for i = 10:89
                clc; disp(i/5);
                filename = 'Attenuation_Results' + string(channel) + '/data_' + string(i/5) + string(amplitude) +'dB.mat';
                load(filename);
                sig = analysisTools.unstack(data);
%                 [sig, ~] = analysisTools.unstack3(data);
                FFT = analysisTools.freqAnalysis(sig, 1024);
                pks.mW(i-9) = analysisTools.conversionmW(max(FFT));
                pks.dBm(i-9) = analysisTools.conversiondBm(max(FFT));
            end 
            % Offset based on Attenuation
            pks.dBmAdj = pks.dBm + attenuation;
            % Determine attenuation setting
            u = fi([],0,6,0);
            pks.attDec = cast(pks.dBmAdj*4, 'like', u);
            pks.attHex = dec2hex(pks.attDec);
            % List corresponding frequencies
            pks.f = 2:0.2:17.8;
        end
        
    end
    methods(Static, Access = public)
        %% Extract logged data
        function data = extractLogData(fileName)
            warning('off');
            dmaLen = 1024;  % Length of each dma output
            % Input logName is <filename>.dat seen in the scope block mask entry
            f = SimulinkRealTime.fileSystem;
            h = fopen(f,fileName);
            data_xpc  = fread(f,h);
            data = SimulinkRealTime.utils.getFileScopeData(data_xpc);
            data = data.data(:,1:dmaLen);
            f.fclose(h);
            warning('on');
        end

        %% Unbuffer and unstack data
        % No Reference Signal - Incorrect Unstacking
        function sig = unstack(data)
            [dmaSamp, dmaLen] = size(data);  % Length of each dma output and Number of dma samples
            unbuffed = zeros(2*dmaLen*dmaSamp/8,8); 
            step = 1; % Stacked signal sample interval
            fprintf("\t Unbuffering Signal ... ");
            for dataStep = 1:dmaSamp
                % For Each Row in Data, convert to binary
                dmaOut = dec2bin(data(dataStep,1:dmaLen),32);
                % Every Stacked Sample
                for dmaIdx = 1:4:dmaLen
                    dmaNext = 0;
                    % Unbuffer the stacked signal into vectors of eight width
                    for sigNum = 1:2:8
                        value = dmaOut(dmaIdx+dmaNext,:); 
                        % Sample of unstacked input signal at the real time step 
                        unbuffed(step,sigNum) = typecast(uint16(bin2dec(value(1:16))),'int16'); % DAC 1,3,5,7 - Low
                        unbuffed(step,sigNum+1) = typecast(uint16(bin2dec(value(17:32))),'int16'); % DAC 2,4,6,8 - High
                        dmaNext = dmaNext + 1;
                    end
                    step = step + 1;
                end
            end
            disp("Finished");
            fprintf("\t Unstacking Signal ... ");
            % Unstack signal to represent the actual input signal where each consecutive index is seperated by the sampling interval
            sig = zeros(size(unbuffed,1)*8,1);
            % Each sample in unbuffered output
            for timeStep = 0:length(sig)-1
    %             sig(timeStep+1) = unbuffed(floor(timeStep/8)+1,mod(timeStep,8)+1);
                % Unstack in reverse for each sample.
                sig(timeStep+1) = unbuffed(floor(timeStep/8)+1,8-mod(timeStep,8));
            end
            disp("Finished");
        end
        % Bad 20s Input/Output Unstack - Won't work with current data
        function [sig, ref] = unstack2(data)
            [dmaSamp, dmaLen] = size(data);  % Length of each dma output and Number of dma samples
            unbuffed_out = zeros(dmaLen*dmaSamp/8,8);
            unbuffed_in = zeros(dmaLen*dmaSamp/8,8);
            step = 1; % Stacked signal sample interval
            fprintf("\t Unbuffering Signal ... ");
            for dataStep = 1:dmaSamp
                dmaOut = dec2bin(data(dataStep,1:dmaLen));
                for dmaIdx = 1:4:dmaLen
                    dmaNext = 0;
                    for sigNum = 1:2:8
                        value = dmaOut(dmaIdx+dmaNext,:); 
                        % Sample of unstacked input signal at the real time step
                        if(mod(dmaIdx, 8) == 1)
                            unbuffed_out(step,sigNum) = typecast(uint16(bin2dec(value(1:16))),'int16'); 
                            unbuffed_out(step,sigNum+1) = typecast(uint16(bin2dec(value(17:32))),'int16');
                        else
                            unbuffed_in(step,sigNum) = typecast(uint16(bin2dec(value(1:16))),'int16'); 
                            unbuffed_in(step,sigNum+1) = typecast(uint16(bin2dec(value(17:32))),'int16');
                        end
                        dmaNext = dmaNext + 1;
                    end
                    step = step + 1;
                end
            end
            unbuffed_out = unbuffed_out(1:2:length(unbuffed_out), :);
            unbuffed_in = unbuffed_in(2:2:length(unbuffed_in), :);
            disp("Finished");
            fprintf("\t Unstacking Signal ... ");
            % Unstack signal to represent the actual input signal where each consecutive index is seperated by the sampling interval
            sig = zeros(size(unbuffed_out,1)*8,1);
            ref = zeros(size(unbuffed_in,1)*8,1);
            for timeStep = 0:length(sig)-1
                sig(timeStep+1) = unbuffed_out(floor(timeStep/8)+1,mod(timeStep,8)+1);
    %             sig(timeStep+1) = unbuffed_out(floor(timeStep/8)+1,8-mod(timeStep,8));
    %             ref(timeStep+1) = unbuffed_in(floor(timeStep/8)+1,mod(timeStep,8)+1);
                ref(timeStep+1) = unbuffed_in(floor(timeStep/8)+1,8-mod(timeStep,8));
            end
            for i = 1:2:length(sig)
               temp = sig(i+1);
               sig(i+1) = sig(i);
               sig(i) = temp;
               temp = ref(i+1);
               ref(i+1) = ref(i);
               ref(i) = temp;
            end
            disp("Finished");
        end
        % Clean 9s Input/Output Unstack - Works 
        function [sig, ref] = unstack3(rawData)
            [dmaSamp, dmaLen] = size(rawData);
            sigL = dmaSamp*dmaLen;
            rawData = reshape(rawData', 4, sigL/4)';
            sigRaw = reshape(rawData(1:2:end, :)', sigL/2, 1);
            refRaw = reshape(rawData(2:2:end, :)', sigL/2, 1);
            sigBin = dec2bin(sigRaw, 32);
            refBin = dec2bin(refRaw, 32);
            sig(:,1) = typecast(uint16(bin2dec(sigBin(:,17:32))),'int16');
            sig(:,2) = typecast(uint16(bin2dec(sigBin(:,1:16))),'int16');
            ref(:,1) = typecast(uint16(bin2dec(refBin(:,17:32))),'int16');
            ref(:,2) = typecast(uint16(bin2dec(refBin(:,1:16))),'int16');
            sig = reshape(sig', sigL, 1);
            ref = reshape(ref', sigL, 1);
        end

        %% Spurious free dynamic range analysis
        function [FFT, fundamental, fine, coarse] = SFDR(sig, FFTlen, msdFine, msdCoarse, fs)
            FFT = analysisTools.freqAnalysis(sig, FFTlen, 2); % Averaged FFT of the signal
            % Find artifact peaks to calculats SFDR
            fprintf("\t Starting SFDR calculations ... ");
            coarse = struct('freq',0,'amplitude',0,'sfdr',0);
            fine = struct('freq',0,'amplitude',0,'sfdr',0);
            fundamental = struct('freq', 0, 'amplitude', 0, 'index', 0);
            % Convert SFDR frequency bounds to indexes
            msdFine = msdFine*FFTlen/fs;
            msdCoarse = msdCoarse*FFTlen/fs;

            % Find peaks between 200 KHz and 5 MHz around the target frequency
            [pks,pkIdx] = findpeaks(FFT);
            [maxVal,maxIdx] = max(FFT);
            % Max amplitude frequency is the fundamental
            fundamental.amplitude = analysisTools.conversiondBm(maxVal);
            fundamental.index = maxIdx;
            fundamental.freq = maxIdx*fs/FFTlen;
            
            peaksFine = zeros(1,2);
            % Determine the peaks inside the fine bounds
            for i = 1:length(pks)
                if (pks(i) > 0 && (pkIdx(i) > maxIdx + msdFine || pkIdx(i) < maxIdx - msdFine) ...
                       && (pkIdx(i) < maxIdx + msdCoarse && pkIdx(i) > maxIdx - msdCoarse))
                    peaksFine(end+1, 1) = pks(i);
                    peaksFine(end, 2) = pkIdx(i);
                end
            end

            % Calculate the SFDR of the spurs close to the target frequency
            [spurValFine, spurIdxFine] = max(peaksFine(:,1));
            fine.amplitude = analysisTools.conversiondBm(spurValFine);
            fine.index = peaksFine(spurIdxFine, 2);
            fine.freq = fine.index*fs/FFTlen;
            fine.sfdr = analysisTools.conversiondBm(maxVal) - fine.amplitude;

            % Find peaks above 5 MHz around the target frequency
            peaksCoarse = zeros(1,2);
            % Determine the peaks outside the fine bounds
            for i = 1:length(pks)
                if (pks(i) > 0 && (pkIdx(i) > maxIdx + msdCoarse || pkIdx(i) < maxIdx - msdCoarse))
                    peaksCoarse(end+1, 1) = pks(i);
                    peaksCoarse(end, 2) = pkIdx(i);
                end
            end

            % Calculate the SFDR of the spurs far from the target frequency
            [spurValCoarse, spurIdxCoarse] = max(peaksCoarse(:,1));
            coarse.amplitude = analysisTools.conversiondBm(spurValCoarse);
            coarse.index = peaksCoarse(spurIdxCoarse, 2);
            coarse.freq = coarse.index*fs/FFTlen;
            coarse.sfdr = analysisTools.conversiondBm(maxVal) - coarse.amplitude;
            disp("Finished");

            % Convert FFT to dBm
            FFT = analysisTools.conversiondBm(FFT);
        end

        %% Settling time analysis
        function [settleTimes, slope, tgtAmp, pks, trghs, pksZero, trghsZero] = settleAnalysis(sig, ref, FFTlen, BW, dec, swSpeed, fs)
            N = length(sig);
            % Scale switch speed to ns
            swSpeed = swSpeed*1e3;
            pks = zeros(floor(N/(2*swSpeed)),2);
            trghs = zeros(floor(N/(2*swSpeed)),2);
            settleTimes = zeros(floor(N/(2*swSpeed)),1);
            swSpeed = swSpeed/2^dec;
            tgtAmp = zeros(N/2,1);              % Array of amplitudes of the target frequency at each FFT frame
    %         tgtIdx = tgtFreq*FFTlen/fs;                 % FFT index corresponding to the target frequency
            BWIdx = BW*FFTlen/fs;                       % FFT index associated with the bandwidth around the target frequency 

            % Determine fundamental frequency index
            [~,tgtIdx] = max(analysisTools.freqAnalysis(ref, FFTlen, 2));

            % Frequency analysis to track amplitude of target frequency
            fprintf("\t Starting Frequency Analysis ... ");
            sig = sig/2^16;
            % Window Definition
            w = hann(FFTlen/2, 'periodic');
            w(1:FFTlen/2^2) = 1;
            w(end-FFTlen/2^2:end) = 1;
            w(1:FFTlen/2^8) = 0;
            w(end-FFTlen/2^8:end) = 0;
            
            % FFT per timestep
            for t = 1:(N/2)
                % Condition FFT
                FFT = fft(sig(t:t+FFTlen-1));
                FFT = abs(FFT/FFTlen);
                % One Sided
                FFT = FFT(FFTlen/2+1:FFTlen);                    
                FFT(2:end-1) = 2*FFT(2:end-1);
                FFT = w.*FFT;
                % Maximum amplitude of primary frequency
                [~, maxIdx] = max(FFT);
                tgtAmp(t) = max(FFT(round(tgtIdx-BWIdx):round(tgtIdx+BWIdx)));
            end
            disp("Finished");
            
            fprintf("\t Smoothing Amplitude Reponse ... ");
            % Average amplitude response of target frequency
            tgtAmp = analysisTools.conversiondBm(tgtAmp);

            temp = [];
            % Moving average for target amplitude
            for t = 1:2^dec:length(tgtAmp)-2^dec
                temp(end+1) = mean(tgtAmp(t:t+2^dec));
            end
            tgtAmp = temp;
            
            % Find the rate of change of target amplitude over time
            slope = diff(tgtAmp(:));
            disp("Finished");
            
            fprintf("\t Finding Settling Time Edges ... ");
            % Find settling time edges
            idx = 1;
            % Find first peak and trough of slope
            [pks(idx, 1), pks(idx, 2)] = max(slope(1:1+swSpeed));
            [trghs(idx, 1), trghs(idx, 2)] = min(slope(1:1+swSpeed));
            % If Peak occurs before trough offset such that unsettled signal is roughly centered
            offset = pks(idx,2) + swSpeed/2;
            % Otherwise don't offset
            if(pks(idx,2) > trghs(idx,2))
                offset = 1;
            end
            for i = offset:swSpeed:length(slope)-swSpeed
               % Find peaks and troughs of slope
               [pks(idx, 1), pks(idx, 2)] = max(slope(i:i+swSpeed));
               [trghs(idx, 1), trghs(idx, 2)] = min(slope(i:i+swSpeed));
               % Find peak and trough indexes
               pks(idx, 2) = pks(idx, 2) + i;
               trghs(idx, 2) = trghs(idx, 2) + i;
               if(pks(idx,2) > trghs(idx,2))
                   % Determine threshold based on surrounding noise
                   thresh = mean(findpeaks(abs(slope(cat(2, i:trghs(idx,2),pks(idx,2):i+swSpeed)))));
                   % If peak is not significant enough, discard it
                   if((pks(idx,1)-trghs(idx,1)) > 7*thresh)
                       idx = idx + 1;
                   end
               end
            end
            
            % Remove trailing zeroes
            pks = pks(1:idx-1,:);
            trghs = trghs(1:idx-1,:);
            pksZero = zeros(idx-1,2);
            trghsZero = zeros(idx-1,2);
            
            % Look at first amplitude interval
            w = tgtAmp(1:trghs(1,2));
            % Find 'zero' based on first peak preceding slope trough
            [~,ttemp] = findpeaks(w);
            trghsZero(1,2) = ttemp(end-1)+1;
            trghsZero(1,1) = tgtAmp(trghsZero(1,2));
            for j = 2:size(pks)
                w = tgtAmp(pks(j-1,2):trghs(j,2));
                [~,ptemp] = findpeaks(w);
                % Find 'zero' based on first peak preceding slope trough
                trghsZero(j,2) = pks(j-1,2) + ptemp(end-1) + 1;
                trghsZero(j,1) = tgtAmp(trghsZero(j,2));
                % Find 'zero' based on first peak after slope peak
                pksZero(j-1,2) = pks(j-1,2) + ptemp(2) - 1;
                pksZero(j-1,1) = tgtAmp(pksZero(j-1,2));
            end
            % Look at last amplitude interval
            w = tgtAmp(pks(end-1):end);
            [~,ptemp] = findpeaks(w);
            % Find 'zero' based on first peak after slope peak
            pksZero(idx-1,2) = pks(idx-1,2) + ptemp(2)-1;
            pksZero(idx-1,1) = tgtAmp(pksZero(idx-1,2));
            
            % Calculate settling time based on zero intervals
            settleTimes(1:idx-1) = (2^dec)*(pksZero(:,2) - trghsZero(:,2))-FFTlen;
    %         settleTimes = (2^dec)*(pksZero((1:idx-1),2) - trghsZero((1:idx-1),2))-FFTlen;
            disp("Finished");
        end
        
        %% Linearity analysis
        function [io, ratio, diff, m, dev] = linearAnalysis(sfull, rfull, s, r)
            % Find amplitude of signal and reference
            disp("1Vp-p");
            io(1, 2) = max(analysisTools.freqAnalysis(sfull, 2^10, 2));
            io(1, 1) = max(analysisTools.freqAnalysis(rfull, 2^10, 2));
            disp("0.75Vp-p");
            io(2, 2) = max(analysisTools.freqAnalysis(s.one, 2^10, 2));
            io(2, 1) = max(analysisTools.freqAnalysis(r.one, 2^10, 2));
            disp("0.5Vp-p");
            io(2, 2) = max(analysisTools.freqAnalysis(s.two, 2^10, 2));
            io(2, 1) = max(analysisTools.freqAnalysis(r.two, 2^10, 2));
            disp("0.375Vp-p");
            io(2, 2) = max(analysisTools.freqAnalysis(s.three, 2^10, 2));
            io(2, 1) = max(analysisTools.freqAnalysis(r.three, 2^10, 2));
            disp("0.25Vp-p");
            io(3, 2) = max(analysisTools.freqAnalysis(s.four, 2^10, 2));
            io(3, 1) = max(analysisTools.freqAnalysis(r.four, 2^10, 2));
            disp("0.125Vp-p");
            io(4, 2) = max(analysisTools.freqAnalysis(s.five, 2^10, 2));
            io(4, 1) = max(analysisTools.freqAnalysis(r.five, 2^10, 2));
            disp("0.0625Vp-p");
            io(5, 2) = max(analysisTools.freqAnalysis(s.six, 2^10, 2));
            io(5, 1) = max(analysisTools.freqAnalysis(r.six, 2^10, 2));
            
            ratio = io(:,2)./io(:,1);
            m = mean(ratio);
            diff = io(:,2) - m*io(:,1);
            [~, dev] = max(abs(diff));
            dev = diff(dev);
        end

        %% Frequency Analysis
        function [FFT] = freqAnalysis(sig, FFTlen, overlap)
            % Scale to Volts
            sig = sig/2^16;
            N = length(sig);
            % Use Welch's method to smooth frequency response and reduce noise
            shift = FFTlen/overlap;
            % If length is greater than the size of the signal, just do 1 FFT
            if(FFTlen > N)
               shift = N; 
            end
            FFTs = zeros(FFTlen/2,1);
            frame = 1;
            fprintf("\t Starting Frequency Analysis ...");
            % Window Definition
            w = hann(FFTlen/2, 'periodic');
            w(1:FFTlen/2^2) = 1;
            w(end-FFTlen/2^2:end) = 1;
            w(1:FFTlen/2^8) = 0;
            w(end-FFTlen/2^8:end) = 0;
            for t = 1:shift:N-FFTlen+1             
                FFT = fft(sig(t:t+FFTlen-1));
                FFT = abs(FFT/FFTlen);
                % One Sided
                FFT = FFT(1:FFTlen/2);
                FFT(2:end-1) = 2*FFT(2:end-1);
                FFT = w.*FFT;
                FFTs(:,frame) = FFT;
                frame = frame + 1;
            end
            FFT = mean(FFTs, 2);
            disp("Finished");
        end

        %% Unit Conversion
        function dBm = conversiondBm(mag)
            mW = analysisTools.conversionmW(mag);
            dBm = 10*log10(mW);
        end
        function mW = conversionmW(mag)
           mW = ((((mag)/sqrt(2)).^2)/50)*1e3; 
        end
        function mag = conversionmag(dBm)
            mag = int16((2^16)*sqrt(10^(dBm/10-1)));
        end
    end
end