classdef analyseData
    methods(Static)       
        %% Initialise target
        function initSLRT()
            global fs
            fs = 1e9;
            global init_pause;
            global dma_pause;
            init_pause = 0;
            dma_pause = 1;
            
            % Connecting to target
            fprintf("Connecting to target ... ")
            tg = slrt;
%             tg.reboot;
%             pause(40);
            
            % Uploading model to target
%             fprintf("Uploading model to target ... ")
%             tg.load('gm_Analysis_slrt');
%             
            % Configuring model parameters
            fprintf("Configuring model parameters ... ")
            tg.StopTime = inf;
            tg.SampleTime = 1e-3;
            tg.setparam('channel_select', 15);
            tg.setparam('trigger_width', 3);
            tg.setparam('gain_control', 2^14-1);
            
            % Flush DDS index
            fprintf("Flushing DDS index... ")
            tg.start;
            tg.setparam('selection_type', 1);
            pause(dma_pause);
            tg.setparam('selection_type', 0);
            pause(dma_pause);
            tg.stop;
            
            disp("Finished")
        end
        
        %% Collect data
        function dataCollect(fileNum, boardSel, description, extra)
            
            global tg;
            global init_pause;
            global dma_pause;
            
            % Flush DDS index (remove this)
            fprintf("Flushing DDS index... ")
            tg.start;
            tg.setparam('selection_type', 1);
            pause(dma_pause);
            tg.setparam('selection_type', 0);
            pause(dma_pause);
            tg.stop;
            
            
            % Initialise data file
            header.testno = string(fileNum);
            header.boardno = string(boardSel);
            header.timestamp = string(datetime(now(),'ConvertFrom','datenum'));
            header.description = description;
            header.centre = extra.centre;
            header.input = extra.input;
            header.switch = extra.switch;
            tg.setparam('board_select', boardSel);
            filename = 'Results/TestDataBrd' + string(boardSel) + '_' + string(header.centre) + 'GHz.mat';
            save(filename, 'header');

            % SFDR data
            fprintf("Collecting SFDR data ... ")
            tg.setparam('gain_control', 2^14-1);
            tg.setparam("dma_enable", 0);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            dataSFDR = analysisTools.extractLogData('DMAdata.dat');
            save(filename, 'dataSFDR', '-append');

            % Setting time data
            fprintf("Collecting settling time data ... ")
            tg.setparam('gain_control', 2^14-1);
            tg.setparam("dma_enable", 0);
            tg.setparam('selection_type', 1);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            dataSettle = analysisTools.extractLogData('DMAdata.dat');
            save(filename, 'dataSettle', '-append');

            % Output linearity data
            fprintf("Collecting amplitude linearity data ... ")
            % 3/4 Amplitude
            tg.setparam("dma_enable", 0);
            tg.setparam('selection_type', 0);
            tg.setparam('gain_control', 3*2^12-1);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            data3_4 = analysisTools.extractLogData('DMAdata.dat');
            save(filename, 'data3_4', '-append');
            
            % Half Amplitude
            tg.setparam("dma_enable", 0);
            tg.setparam('selection_type', 0);
            tg.setparam('gain_control', 2^13-1);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            dataHalf = analysisTools.extractLogData('DMAdata.dat');
            save(filename, 'dataHalf', '-append');
            
            % 3/8 Amplitude
            tg.setparam("dma_enable", 0);
            tg.setparam('selection_type', 0);
            tg.setparam('gain_control', 3*2^11-1);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            data3_8 = analysisTools.extractLogData('DMAdata.dat');
            save(filename, 'data3_8', '-append');

            % Quarter Amplitude
            tg.setparam('selection_type', 0);
            tg.setparam('gain_control', 2^12-1);
            tg.setparam("dma_enable", 0);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            dataQuarter = analysisTools.extractLogData('DMAdata.dat');
            save(filename, 'dataQuarter', '-append');

            % Eighth Amplitude
            tg.setparam('selection_type', 0);
            tg.setparam('gain_control', 2^11-1);
            tg.setparam("dma_enable", 0);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            dataEighth = analysisTools.extractLogData('DMAdata.dat');
            save(filename, 'dataEighth', '-append');

            % 1/16th Amplitude
            tg.setparam('selection_type', 0);
            tg.setparam('gain_control', 2^10-1);
            tg.setparam("dma_enable", 0);
            tg.start;
            pause(init_pause);
            tg.setparam("dma_enable", 1);
            pause(dma_pause);
            tg.stop;
            dataSixteenth = analysisTools.extractLogData('DMAdata.dat'); 
            save(filename, 'dataSixteenth', '-append');

            analysisTools.init()
            disp("Finished");
        end
        
        %% Spurious Free Dynamic Range (SFDR) Analysis
        function SFDRTest(sigSFDR, filename)
            fprintf("--------------------------------------- <strong>SFDR Analysis</strong> --------------------------------------- \n");
            %% Run analysis
            global fs;
            fs = 1e9;
            [FFT, fundamental, fine, coarse] = analysisTools.SFDR(sigSFDR, 2^18, 0, 5e6);
            filename = strcat('Results/SFDR_', filename); 
            save(filename, 'FFT');
            save(filename, 'fundamental', '-append');
            save(filename, 'fine', '-append');
            save(filename, 'coarse', '-append');
            %% Plot
            FFTlen = length(FFT);
            f = figure();
            f.Position = [100 100 1000 800];

            s = 1;
            e = FFTlen;
            freq =(s:e)*fs/FFTlen/1e6/2;
            subplot(2, 2, 1:2); hold on
            if(coarse.amplitude >= -120)
                rectangle('Position',[1 coarse.amplitude 500 (fundamental.amplitude-coarse.amplitude)], 'FaceColor', '#FFEFD6', 'LineStyle', 'none');
                plot(coarse.freq/1e6, coarse.amplitude, 'v', 'Color', [0.8500 0.3250 0.0980]);
            end
            plot(fundamental.freq/1e6, fundamental.amplitude, 'v', 'Color', [0.6350 0.0780 0.1840]); hold on
            plot(freq, FFT(s:e), 'Color', 'Blue'); 
            axis([freq(1) freq(end) -120 (max(FFT)+20)])
            title('SFDR')
            xlabel('Frequency (MHz)')
            ylabel('Amplitude (dBm)')
            ratio = max(FFT)+ 80;
            text(fundamental.freq/1e6, fundamental.amplitude+0.05*ratio, "\textbf{Fundamental:}" + string(round(fundamental.amplitude,2)), 'HorizontalAlignment','center', 'Interpreter', 'latex') 
            dim = [0.69, 0.815, 0.6, 0.1];
            str = "SFDR ($<\pm$ 5 MHz): " + string(round(fine.sfdr, 2));
            annotation('textbox', 'String', str, 'Position', dim, 'FontWeight', 'bold', 'FitBoxToText', 'on','FontSize', 12, 'LineStyle', 'none', 'HorizontalAlignment','left', 'Interpreter', 'latex');
            dim = [0.69, 0.79, 0.6, 0.1];
            str = "SFDR ($>\pm$ 5 MHz): " + string(round(coarse.sfdr,2));
            annotation('textbox', 'String', str, 'Position', dim, 'FontWeight', 'bold', 'FitBoxToText', 'on', 'FontSize', 12, 'LineStyle', 'none', 'HorizontalAlignment','left', 'Interpreter', 'latex'); 
            
%             s = int32(FFTlen/2 -(abs(FFTlen/2-fine.index))*1.3); if(s < 0) s = 1; end
            s = floor((49*FFTlen)/100);
%             e = int32(FFTlen/2 +(abs(FFTlen/2-fine.index))*1.3); if(e > FFTlen) e = FFTlen; end
            e = ceil((51*FFTlen)/100);
            freq =(double(s):double(e))*fs/FFTlen/1e6/2;
            subplot(2, 2, 3); hold on
            plot(freq, FFT(s:e), 'Color', 'Blue');
            if(fine.amplitude >= -120)
                plot(fine.freq/1e6, fine.amplitude, 'v', 'Color', [0.8500 0.3250 0.0980]);
                axis([freq(1) freq(end) (FFT(s)-5) (fundamental.amplitude + 5)])
            end
            title('Within 10MHz Span')
            xlabel('Frequency (MHz)')
            ylabel('Amplitude (dBm)')
            ratio = fine.amplitude+70;
            if(fine.amplitude >= -120)
                text(fine.freq/1e6, fine.amplitude+0.075*ratio, "\textbf{Spur:} \\" + string(round(fine.amplitude,2)), 'HorizontalAlignment','center', 'Interpreter', 'latex')
            end
            
            s = int32(FFTlen/2 -(abs(FFTlen/2-coarse.index))*1.3); if(s < 0) s = 1; end
            e = int32(FFTlen/2 +(abs(FFTlen/2-coarse.index))*1.3); if(e > FFTlen) e = FFTlen; end
            freq =(double(s):double(e))*fs/FFTlen/1e6/2;
            subplot(2,2,4); hold on
            plot(freq, FFT(s:e), 'Color', 'Blue');
            if(coarse.amplitude >= -120)
                plot(coarse.freq/1e6, coarse.amplitude, 'v', 'Color', [0.8500 0.3250 0.0980]);
                axis([freq(1) freq(end) -120 (fundamental.amplitude + 5)])
            end
            title('Outside 10MHz Span')
            xlabel('Frequency (MHz)')
            ylabel('Amplitude (dBm)')
            if(coarse.amplitude >= -120)
                text(coarse.freq/1e6, coarse.amplitude+ 0.075*ratio, "\textbf{Spur:} \\" + string(round(coarse.amplitude,2)), 'HorizontalAlignment','center', 'Interpreter', 'latex')
            end
            % Display SFDR
            fprintf("<strong>SFDR(<5MHz):</strong> %2.2f\n", fine.sfdr);
            fprintf("<strong>SFDR(>5MHz):</strong> %2.2f\n", coarse.sfdr);
            disp("--------------------------------------- <strong>Complete</strong> ---------------------------------------");
        end   
        %% Settling Time Analysis
        function SettleTimeTest(dataSettle, filename)
            fprintf("--------------------------------------- <strong>Settling Time Analysis</strong> --------------------------------------- \n");
            % Run analysis
            dec = 1;
            global fs;
            fs = 1e9;
            [sigSettle, refSettle] = analysisTools.unstack2(dataSettle);
%             [sigSettle, refSettle] = analysisTools.unstack3(dataSettle);
            [settleTimes, slope, tgtAmp, pks, trghs, pksZero, trghsZero] = analysisTools.settleAnalysis2(sigSettle, refSettle, 2^10, 0, dec, 10);
            filename = strcat('Results/SettlingTime_', filename); 
            save(filename, 'settleTimes');
            save(filename, 'slope', '-append');
            save(filename, 'tgtAmp', '-append');
            save(filename, 'pks', '-append');
            save(filename, 'trghs', '-append');
            save(filename, 'pksZero', '-append');
            save(filename, 'trghsZero', '-append');
%             tgtAmp = tgtAmp(1:(length(sigSettle)/4)-1);
%             slope = slope(1:(length(sigSettle)/4)/(2^dec)-1);
%             load('TestResultsBrd1_1.mat');
            time = (1:length(tgtAmp))*(2^dec);
            timeSlope = (1:length(slope))*(2^dec);

            % Plot
            f2 = figure();
            f2.Position = [100 100 1000 800];

            y = subplot(2,1,1);
            plot(time, tgtAmp, pksZero(:,2)*(2^dec), pksZero(:,1), 'g*', trghsZero(:,2)*(2^dec), trghsZero(:,1), 'm*');
            title('Amplitude of fundamental frequency over time: 250 MHz')
            xlabel('Time (ns)')
            ylabel('Amplitude (dBm)')
            axis([timeSlope(1) timeSlope(end) floor(min(tgtAmp)) ceil(max(tgtAmp))])

            dy = subplot(2,1,2);
            plot(timeSlope, slope, pks(:,2)*(2^dec), pks(:,1), 'r*', trghs(:,2)*(2^dec), trghs(:,1), 'b*');
            axis([timeSlope(1) timeSlope(end) 1.2*min(slope) 1.2*max(slope)])
            title('Slope of fundamental frequency over time: 250 MHz')
            xlabel('Time (ns)')
            ylabel('Amplitude per Nanosecond (dBm/ns)')
            xlim([0,timeSlope(end)]);
            linkaxes([y, dy], 'x');
            
            % Display average settling time
            fprintf("<strong>Average settling time:</strong> %4.2f ns. \n", mean(settleTimes))
            for i = 1:length(settleTimes)
               if settleTimes(i) < 0
                   disp("Failed");
                   return; 
               end
            end
            disp("--------------------------------------- <strong>Complete</strong> ---------------------------------------");
        end
        
        %% Linearity Analysis
        function LinearityTest(sigSFDR, refSFDR, data3_4, dataHalf, data3_8, dataQuarter, dataEighth, dataSixteenth, filename)
            disp("0.75Vp-p");
            [sig.one, ref.one] = analysisTools.unstack2(data3_4);
%             [sig.one, ref.one] = analysisTools.unstack3(data3_4);
            disp("0.5Vp-p");
            [sig.two, ref.two] = analysisTools.unstack2(dataHalf);
%             [sig.two, ref.two] = analysisTools.unstack3(dataHalf);
            disp("0.375Vp-p");
            [sig.three, ref.three] = analysisTools.unstack2(data3_8);
%             [sig.three, ref.three] = analysisTools.unstack3(data3_8);
            disp("0.25Vp-p");
            [sig.four, ref.four] = analysisTools.unstack2(dataQuarter);
%             [sig.four, ref.four] = analysisTools.unstack3(dataQuarter);
            disp("0.125Vp-p");
            [sig.five, ref.five] = analysisTools.unstack2(dataEighth);
%             [sig.five, ref.five] = analysisTools.unstack3(dataEighth);
            disp("0.0625Vp-p");
            [sig.six, ref.six] = analysisTools.unstack2(dataSixteenth);
%             [sig.six, ref.six] = analysisTools.unstack3(dataSixteenth);
            ratio = analysisTools.linearAnalysis(sigSFDR, refSFDR, sig, ref);
            filename = strcat('Results/Linearity_', filename); 
            save(filename, 'ratio');
            figure();
            title('Voltage Input vs Voltage Output')
            xlabel('Voltage Output (V)')
            ylabel('Voltage Input (V)')
            hold on;
            plot(ratio(:,2), ratio(:,1));
            axis([0 0.25 0 0.25]);
        end
        
        function pks = attenuation_calibration(channel, amplitude, attenuation)
            if(isempty(attenuation))
                attenuation = 0;
            end
            for i = 10:89
                j = i/5; 
                disp(j);
                filename = 'Attenuation_Results' + string(channel) + '/data_' + string(j) + string(amplitude) +'dB.mat';
                load(filename);
                disp("Loaded");
                sig = analysisTools.unstack(data);
                disp("Unstacked");
                FFTlen = 1024;
                FFT = analysisTools.freqAnalysis(sig, FFTlen);
                disp("FFT");
                pks.mW(i-9) = analysisTools.conversionmW(max(FFT));
                pks.dBm(i-9) = analysisTools.conversiondBm(max(FFT));
            end 
            pks.dBmAdj = pks.dBm + attenuation;
            u = fi([],0,6,0);
            pks.attDec = cast(pks.dBmAdj*4, 'like', u);
            pks.attHex = dec2hex(pks.attDec);
            pks.f = 2:0.2:17.8;
        end
    end
end