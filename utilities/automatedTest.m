%% Initialisation
clc;
analyseData.initSLRT();

%% Data Collection
for boardSel = 1:3
    fprintf("------------------------ <strong>Collecting data from board</strong> %d --------------------------- \n", boardSel)
    for fileNum = 9:9
       switch(fileNum)
           case 1
                freq = 2e9;
           case 2
                freq = 3e9;
           case 3
                freq = 4e9;
           case 4
                freq = 6e9;
           case 5
                freq = 8e9;
           case 6
                freq = 10e9;
           case 7
                freq = 12e9;
           case 8
                freq = 14e9;
           case 9
                freq = 16e9;
       end
       description = headerDes(tg, freq);
       RFUDCtools.frequency_set(freq, boardSel);
       analyseData.dataCollect('boardSel', boardSel, 'description', description, 'extra', header);
       clearvars -except fileNum boardSel tg
    end
    disp("-------------------------------------------- Finished -----------------------------------")
    RFUDCtools.powerDown(boardSel);
    tg.reboot;
    pause(30);
    tg.load('gm_Analysis_ReferencedInput_slrt');
end
%% Results Generation
directory = 'Data/';
dat = dir(directory);
dat = dat(3:end);
dat = dat(10:end);
% dat = dat(1);

for i = 1:length(dat)
    filename = strcat(directory, dat(i).name);
    load(filename);
    fprintf("" + header.description + "\n");
    analysisTools.LinearityTest(dataSFDR, data3_4, dataHalf, data3_8, dataQuarter, dataEighth, dataSixteenth, dat(i).name(9:end-4));
    figname = strcat("Figures/Linearity_", dat(i).name(9:end-4));
    savefig(figname);
%     analysisTools.SFDRTest('data', dataSFDR, 'filename', dat(i).name(9:end-4));
%     figname = strcat("Figures/SFDR_", dat(i).name(9:end-4));
% %     savefig(figname);
%     analysisTools.SettleTimeTest('data', dataSettle, 'filename', dat(i).name(9:end-4));
%     figname = strcat("Figures/SettlingTime_", dat(i).name(9:end-4));
%     savefig(figname);
    close all;
    clc;
end
%% Publish Report
if(true)
options.format = 'html';
options.showCode = false;
fprintf("Publishing ... ");
publish('analysis_report.m',options);
disp("Finished");
close all
end
%% Description 
function [description, header] = headerDes(tg, freq)
   header.centre = freq/1e9;
   description = strcat("Band: ", string(freq/1e9));
   description = strcat(description,"GHz, DDS Switch: ");
   header.switch = tg.getparam('switching_period_us');
   description = strcat(description, string(header.switch));
   description = strcat(description,"us, Centre Frequency: ");
   header.input = tg.getparam('channel_select')*10 + 100;
   description = strcat(description, string(header.input));
   description = strcat(description,"MHz, Channel ");
   description = strcat(description, string(tg.getparam('board_select')));
end