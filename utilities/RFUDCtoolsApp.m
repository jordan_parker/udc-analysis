classdef RFUDCtoolsApp < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        UIFigure                       matlab.ui.Figure
        COMPortDropDownLabel           matlab.ui.control.Label
        COMPortDropDown                matlab.ui.control.DropDown
        DDSTestMode                    matlab.ui.control.CheckBox
        DDSTable                       matlab.ui.control.Table
        Channel1Panel                  matlab.ui.container.Panel
        CentreFrequencyHzLabel         matlab.ui.control.Label
        CentreFrequency1               matlab.ui.control.NumericEditField
        AttenuationTxEditFieldLabel    matlab.ui.control.Label
        AttenuationTx1                 matlab.ui.control.NumericEditField
        AttenuationRxEditFieldLabel    matlab.ui.control.Label
        AttenuationRx1                 matlab.ui.control.NumericEditField
        c1ind                          matlab.ui.control.Lamp
        t1ind                          matlab.ui.control.Lamp
        r1ind                          matlab.ui.control.Lamp
        Channel2Panel                  matlab.ui.container.Panel
        CentreFrequencyHzLabel_2       matlab.ui.control.Label
        CentreFrequency2               matlab.ui.control.NumericEditField
        AttenuationTxEditField_2Label  matlab.ui.control.Label
        AttenuationTx2                 matlab.ui.control.NumericEditField
        AttenuationRxEditField_2Label  matlab.ui.control.Label
        AttenuationRx2                 matlab.ui.control.NumericEditField
        c2ind                          matlab.ui.control.Lamp
        t2ind                          matlab.ui.control.Lamp
        r2ind                          matlab.ui.control.Lamp
        Channel3Panel                  matlab.ui.container.Panel
        CentreFrequencyHzLabel_3       matlab.ui.control.Label
        CentreFrequency3               matlab.ui.control.NumericEditField
        AttenuationTxEditField_3Label  matlab.ui.control.Label
        AttenuationTx3                 matlab.ui.control.NumericEditField
        AttenuationRxEditField_3Label  matlab.ui.control.Label
        AttenuationRx3                 matlab.ui.control.NumericEditField
        c3ind                          matlab.ui.control.Lamp
        t3ind                          matlab.ui.control.Lamp
        r3ind                          matlab.ui.control.Lamp
        Image                          matlab.ui.control.Image
        RFUpDownConverterLabel         matlab.ui.control.Label
        DDSHzLabel                     matlab.ui.control.Label
        DDS                            matlab.ui.control.NumericEditField
        DDSIndicator                   matlab.ui.control.Lamp
        ResetButton                    matlab.ui.control.Button
    end

    
    properties (Access = private)
        port_obj % Description
        DDS_table_default
        DDS_table_running
        at1table
        at2table
        at3table
        ar1table
        ar2table
        ar3table
    end
    
    methods (Access = private)
        
        function disable(app)
            app.CentreFrequency1.Enable = 0;
            app.CentreFrequency2.Enable = 0;
            app.CentreFrequency3.Enable = 0;
            app.AttenuationRx1.Enable = 0;
            app.AttenuationRx2.Enable = 0;
            app.AttenuationRx3.Enable = 0;
            app.AttenuationTx1.Enable = 0;
            app.AttenuationTx2.Enable = 0;
            app.AttenuationTx3.Enable = 0;
            app.DDSTable.Enable = 'off';
            app.DDSTestMode.Enable = 0;
            app.DDSTestMode.Value = 0;
            app.CentreFrequency1.Value = 0;
            app.CentreFrequency2.Value = 0;
            app.CentreFrequency3.Value = 0;
            app.AttenuationRx1.Value = 0;
            app.AttenuationRx2.Value = 0;
            app.AttenuationRx3.Value = 0;
            app.AttenuationTx1.Value = 0;
            app.AttenuationTx2.Value = 0;
            app.AttenuationTx3.Value = 0;
            app.DDSTable.Data = app.DDS_table_default;
            app.DDS.Enable = 0;
            app.DDS.Value = 0;
            app.DDS.Visible = 0;
            app.DDSHzLabel.Visible = 0;
            app.DDSIndicator.Visible = 0;
            app.c1ind.Visible = 0;
            app.c2ind.Visible = 0;
            app.c3ind.Visible = 0;
            app.t1ind.Visible = 0;
            app.t2ind.Visible = 0;
            app.t3ind.Visible = 0;
            app.r1ind.Visible = 0;
            app.r2ind.Visible = 0;
            app.r3ind.Visible = 0;
        end
        function enable(app)
            app.CentreFrequency1.Enable = 1;
            app.CentreFrequency2.Enable = 1;
            app.CentreFrequency3.Enable = 1;
            app.AttenuationRx1.Enable = 1;
            app.AttenuationRx2.Enable = 1;
            app.AttenuationRx3.Enable = 1;
            app.AttenuationTx1.Enable = 1;
            app.AttenuationTx2.Enable = 1;
            app.AttenuationTx3.Enable = 1;
            app.DDSTable.Enable = 'on';
            app.DDSTestMode.Enable = 1;
            app.DDS.Enable = 1;
        end
        
        function [tx, rx] = attlookup(app, freq, channel)
            ratio = freq/2e8;
            lower = floor(ratio);
            ratio = ratio - lower;
	    if(channel == 1)
                a = app.at1table(lower-9);
	        b = app.at1table(lower-8);
                c = app.ar1table(lower-9);
	        d = app.ar1table(lower-8);
            elseif(channel == 2)
                a = app.at2table(lower-9);
	        b = app.at2table(lower-8);
                c = app.ar2table(lower-9);
	        d = app.ar2table(lower-8);
            elseif(channel == 3)
                a = app.at3table(lower-9);
	        b = app.at3table(lower-8);
                c = app.ar3table(lower-9);
	        d = app.ar3table(lower-8);
            else
                a = 0;
                b = 0;
                c = 0;
                d = 0;
            end
            tx = a + ratio*(b-a);
            rx = c + ratio*(d-c);
        end
    end
  

    % Callbacks that handle component events
    methods (Access = private)

        % Code that executes after component creation
        function startupFcn(app)
            list = 'None';
            list = cat(1, list, seriallist);
            app.COMPortDropDown.Items = list;
            app.COMPortDropDown.Value = list(1);
            
            app.DDS.Visible = 0;
            app.DDSHzLabel.Visible = 0;
            disable(app);
            
            app.port_obj = 0;
            app.DDS_table_default = [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19  ...
                        20 21 22 23 24 25 26 27 28 29 30 31 32 33 34; 500e6   ... 
                        510e6 520e6 530e6 540e6 550e6 560e6 570e6 580e6 590e6 600e6 610e6 620e6 630e6 ...
                        640e6 650e6 660e6 670e6 680e6 690e6 700e6 710e6 720e6 730e6 740e6 750e6 760e6 ...
                        770e6 780e6 790e6 800e6 810e6 820e6 830e6 840e6]';
            app.DDS_table_running = app.DDS_table_default;
            app.DDSTable.Data = app.DDS_table_default;
            
            app.at1table = [9.5 9.75 10.25 11 11 10.75 10.5 10.75 9.5 10 10 9.25 9.5 ...
                8 7.5 7.75 6.75 6.25 5 0 1.25 2.25 3.75 7.75 10 11.25 11 6.25 2.25 0 ...
                0 0 0 0 0 0 1.5 0 0 0 1.5 0 1.75 0 0 0 0 0 0 0 1]; app.at1table(55:81) = 0;
            app.at2table = [0.75 0 0 0.75 0.75 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 4 6 6 ...
                10.5 10.5 13 9.5 4.75 6.5 3.75 2 2.5 3.25 3.25 3.25 2.5 0.5 0.75 3.5 ...
                2.75 6 3.25 4.5 5.75 6.75 5.25 3.25 0 0 3.5];app.at2table(55:81) = 0;
            app.at3table = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1.5 2.5 3.75 8.25 ...
                9.75 12 13.5 10.75 8 6.5 3.25 1.25 0 1.5 1 1.25 1.25 1.5 4.5 10 13.5 ...
                11.5 10 7.75 9.5 10 5.25 3.25 0 2.5 1.25 2.25 3.5 3.5 1.5 0 0 0 0 1.25 ...
                0.5 0.5 1.75 2 0 0 0 0 0 0 0 0 0 0 0 0 2.25 3.25 2.25 0 0];
            app.ar1table = [10.5 10 10.75 10 11.25 14 10.5 11.75 13.5 12.25 13 13.75 ...
                12 13.25 14.25 12.75 12.75 13.75 13.25 10.75 14 13.75 ...
                14.75 14 10.75 11.25 12.5 11.5 10.25 12 12.25 9.25 10 11.75 8.75 11 ... 
                9.75 11 12 6 11 9 7 9.5 7 8.25 7 9.25 5.75 8.5 8 5 9.75 7.75 8 8.5 ...
                6.25 3 6.5 5 4.25 6.25 5.25 3.75 0.25 3.75 0 0 0.25 2.5 0 0 0.75 0 ...
                0 0 1.25 0 0 0 0];
            app.ar2table = [10.75 10 10 10 11 12.75 8.75 11.75 12 11.5 10.75 12.5 ...
                11.75 11 11.5 10.25 9.5 8.75 7.75 13.25 15 13.25 15 13.25 11 11.75 ...
                11.75 11.5 10.25 11.75 11.5 10.25 11.25 12.25 11.25 11.25 10.5 ...
                10.5 10 8.75 10.75 9.5 8.25 10.5 9.25 6.5 8.75 7.5 5.75 8.5 6.5 6.75 ...
                5.25 5.5 3.75 4.25 2.25 3.5 4.75 3.75 3 3 4.75 0.75 1.75 4 0 0 2.75 ...
                3.5 0 0 1.5 0 0 0 0 0 0 0 0];
            app.ar3table = [10 8.75 10.25 9.25 11 11.5 10 10.25 10.75 10 11.25 11.75 ...
                11 11.25 11 9.75 9.25 10 8.5 13.5 13.5 15.5 15.75 13.5 11.75 11.5 12 ...
                12.75 10.75 12.25 12.25 9.75 12.25 11.5 11.25 12 10.25 11.5 11 5.25 ...
                11.25 10.25 9 11 9.75 9.5 9.25 10 9 9.75 11.25 6.75 6.25 9 9.25 10.25 ...
                7 1.75 7 4.75 6.25 7.25 6 3.5 4.5 6.5 2.5 0 2 4.5 0 0 3.5 0 0 5 6.75 ...
                2 5.5 0 0];
        end

        % Value changed function: COMPortDropDown
        function COMPortDropDownValueChanged(app, event)
            value = app.COMPortDropDown.Value;
            temp = string(value);
            if(temp == 'None')
                disable(app);
                fclose(app.port_obj);
                app.port_obj = 0;
            else
                if(app.port_obj ~= 0)
                    fclose(app.port_obj);
                    app.port_obj = 0;
                end
                s = serial(temp);
                s.BaudRate = 9600;
                s.Terminator = 'CR';
                app.port_obj = s;
                fopen(app.port_obj);
                enable(app);
            end
        end

        % Value changed function: DDSTestMode
        function DDSTestModeValueChanged(app, event)
            value = app.DDSTestMode.Value;
            if(value == 1)
                app.DDSTestMode.Enable = 0;
                app.DDS.Visible = 1;
                app.DDSHzLabel.Visible = 1;
            else
                app.DDSTestMode.Enable = 1;
                app.DDS.Visible = 0;
                app.DDSHzLabel.Visible = 0;
            end
        end

        % Close request function: UIFigure
        function UIFigureCloseRequest(app, event)
            if(app.port_obj ~= 0)
                fclose(app.port_obj);
            end
            delete(app)
        end

        % Cell edit callback: DDSTable
        function DDSTableCellEdit(app, event)
            indices = event.Indices;
            newData = event.NewData;
            if(newData < 500e6 || newData > 850e6)
                app.DDSTable.Data(indices(1),indices(2)) = app.DDS_table_running(indices(1),indices(2));
            else
               cmd = strcat("f:up:", string(indices(1)-1), ":", string(newData));
               disp(cmd);
%                fprintf(app.port_obj, cmd);
               cmd = strcat("f:down:", string(indices(1)-1), ":", string(1.35e9 - newData));
               disp(cmd);
%                fprintf(app.port_obj, cmd);
               app.DDS_table_running(indices(1),indices(2)) = app.DDSTable.Data(indices(1),indices(2));
            end
        end

        % Value changed function: DDS
        function DDSValueChanged(app, event)
            value = app.DDS.Value;
            app.DDSIndicator.Visible = 1;
            app.DDSIndicator.Color = [1.00,0.00,0.00];
            if(value <= 850e6 && value >= 500e6)
%                 fprintf(app.port_obj, 't:en');
                cmd = strcat("t:d:", string(value));
                disp(cmd);
%                 fprintf(app.port_obj, cmd);
                app.DDSIndicator.Color = [0.00,1.00,0.00];
            end
        end

        % Value changed function: CentreFrequency1
        function CentreFrequency1ValueChanged(app, event)
            value = app.CentreFrequency1.Value;
            app.c1ind.Visible = 1;
            app.c1ind.Color = [1,0,0];
            if(value >= 2e9 && value <= 17.825e9)
                cmd = strcat('b:', string(value/1e3),':0:3');
                disp(cmd);
%                 fprintf(app.port_obj, cmd);
                app.c1ind.Color = [0,1,0];
                [tx, rx]= attlookup(app, value, 1);
                app.AttenuationTx1.Value = tx;
                app.AttenuationRx1.Value = rx;
            end
            
        end

        % Value changed function: CentreFrequency2
        function CentreFrequency2ValueChanged(app, event)
            value = app.CentreFrequency2.Value;
            app.c2ind.Visible = 1;
            app.c2ind.Color = [1,0,0];
            if(value >= 2e9 && value <= 17.825e9)
                cmd = strcat('b:', string(value/1e3), '0:2');
                disp(cmd);
%                 fprintf(app.port_obj, cmd);
                app.c2ind.Color = [0,1,0];
                [tx, rx]= attlookup(app, value, 2);
                app.AttenuationTx2.Value = tx;
                app.AttenuationRx2.Value = rx;            
            end
            
        end

        % Value changed function: CentreFrequency3
        function CentreFrequency3ValueChanged(app, event)
            value = app.CentreFrequency3.Value;
            app.c3ind.Visible = 1;
            app.c3ind.Color = [1,0,0];
            if(value >= 2e9 && value <= 17.825e9)
                cmd = strcat('b:', string(value/1e3),':0:1');
                disp(cmd);
%                 fprintf(app.port_obj, cmd);
                app.c3ind.Color = [0,1,0];
                [tx, rx]= attlookup(app, value, 3);
                app.AttenuationTx3.Value = tx;
                app.AttenuationRx3.Value = rx;
            end
            
        end

        % Value changed function: AttenuationTx1
        function AttenuationTx1ValueChanged(app, event)
            value = app.AttenuationTx1.Value;
            value = floor(value*4);
            app.t1ind.Visible = 1;
            app.t1ind.Color = [1,0,0];
            if(value < 63 && value > 0)
                cmd = strcat('c:t:', string(value), ':3');
                disp(cmd);
%                 fprintf(app.port_obj, cmd);
                app.t1ind.Color = [0,1,0];
            end
            
        end

        % Value changed function: AttenuationTx2
        function AttenuationTx2ValueChanged(app, event)
            value = app.AttenuationTx2.Value;
            value = floor(value*4);
            app.t2ind.Visible = 1;
            app.t2ind.Color = [1,0,0];
            if(value < 63 && value > 0)
                cmd = strcat('c:t:', string(value), ':2');
                disp(cmd);
%                 fprintf(app.port_obj, cmd);
                app.t2ind.Color = [0,1,0];
            end
        end

        % Value changed function: AttenuationTx3
        function AttenuationTx3ValueChanged(app, event)
            value = app.AttenuationTx3.Value;
            value = floor(value*4);
            app.t3ind.Visible = 1;
            app.t3ind.Color = [1,0,0];
            if(value < 63 && value > 0)
                cmd = strcat('c:t:', string(value), ':1');
                disp(cmd);
%                 fprintf(app.port_obj, cmd);
                app.t3ind.Color = [0,1,0];
            end
        end

        % Value changed function: AttenuationRx1
        function AttenuationRx1ValueChanged(app, event)
            value = app.AttenuationRx1.Value;
            value = floor(value*4);
            app.r1ind.Visible = 1;
            app.r1ind.Color = [1,0,0];
            if(value < 63 && value > 0)
                cmd = strcat('c:r:', string(value), ':3');
                disp(cmd);
%                 fprintf(app.port_obj, cmd);
                app.r1ind.Color = [0,1,0];
            end
        end

        % Value changed function: AttenuationRx2
        function AttenuationRx2ValueChanged(app, event)
            value = app.AttenuationRx2.Value;
            value = floor(value*4);
            app.r2ind.Visible = 1;
            app.r2ind.Color = [1,0,0];
            if(value < 63 && value > 0)
                cmd = strcat('c:r:', string(value), ':2');
                disp(cmd);
%                 fprintf(app.port_obj, cmd);
                app.r2ind.Color = [0,1,0];
            end
        end

        % Value changed function: AttenuationRx3
        function AttenuationRx3ValueChanged(app, event)
            value = app.AttenuationRx3.Value;
            value = floor(value*4);
            app.r3ind.Visible = 1;
            app.r3ind.Color = [1,0,0];
            if(value < 63 && value > 0)
                cmd = strcat('c:r:', string(value), ':1');
                disp(cmd);
%                 fprintf(app.port_obj, cmd);
                app.r3ind.Color = [0,1,0];
            end
        end

        % Button pushed function: ResetButton
        function ResetButtonValueChanged(app, event)
            disable(app);
            enable(app);
        end
    end

    % Component initialization
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create UIFigure and hide until all components are created
            app.UIFigure = uifigure('Visible', 'off');
            app.UIFigure.Color = [1 1 1];
            app.UIFigure.Position = [100 100 634 530];
            app.UIFigure.Name = 'UI Figure';
            app.UIFigure.CloseRequestFcn = createCallbackFcn(app, @UIFigureCloseRequest, true);

            % Create COMPortDropDownLabel
            app.COMPortDropDownLabel = uilabel(app.UIFigure);
            app.COMPortDropDownLabel.BackgroundColor = [1 1 1];
            app.COMPortDropDownLabel.HorizontalAlignment = 'right';
            app.COMPortDropDownLabel.Position = [28 396 59 22];
            app.COMPortDropDownLabel.Text = 'COM Port';

            % Create COMPortDropDown
            app.COMPortDropDown = uidropdown(app.UIFigure);
            app.COMPortDropDown.Items = {};
            app.COMPortDropDown.ValueChangedFcn = createCallbackFcn(app, @COMPortDropDownValueChanged, true);
            app.COMPortDropDown.BackgroundColor = [1 1 1];
            app.COMPortDropDown.Position = [102 396 100 22];
            app.COMPortDropDown.Value = {};

            % Create DDSTestMode
            app.DDSTestMode = uicheckbox(app.UIFigure);
            app.DDSTestMode.ValueChangedFcn = createCallbackFcn(app, @DDSTestModeValueChanged, true);
            app.DDSTestMode.Text = 'DDS Test Mode';
            app.DDSTestMode.Position = [6 357 106 22];

            % Create DDSTable
            app.DDSTable = uitable(app.UIFigure);
            app.DDSTable.ColumnName = {'Index'; 'DDS Frequency (Hz)'};
            app.DDSTable.ColumnWidth = {50, 'auto'};
            app.DDSTable.RowName = {};
            app.DDSTable.ColumnEditable = [false true];
            app.DDSTable.CellEditCallback = createCallbackFcn(app, @DDSTableCellEdit, true);
            app.DDSTable.Tooltip = {'DDS Frequency must be between 500 and 850 MHz'};
            app.DDSTable.Position = [78 14 190 333];

            % Create Channel1Panel
            app.Channel1Panel = uipanel(app.UIFigure);
            app.Channel1Panel.Title = 'Channel 1';
            app.Channel1Panel.BackgroundColor = [1 1 1];
            app.Channel1Panel.FontWeight = 'bold';
            app.Channel1Panel.FontSize = 16;
            app.Channel1Panel.Position = [325 357 299 162];

            % Create CentreFrequencyHzLabel
            app.CentreFrequencyHzLabel = uilabel(app.Channel1Panel);
            app.CentreFrequencyHzLabel.HorizontalAlignment = 'right';
            app.CentreFrequencyHzLabel.Position = [11 93 128 22];
            app.CentreFrequencyHzLabel.Text = 'Centre Frequency (Hz)';

            % Create CentreFrequency1
            app.CentreFrequency1 = uieditfield(app.Channel1Panel, 'numeric');
            app.CentreFrequency1.ValueChangedFcn = createCallbackFcn(app, @CentreFrequency1ValueChanged, true);
            app.CentreFrequency1.Tooltip = {'Centre Frequency must be between 2-17.825GHz'};
            app.CentreFrequency1.Position = [154 93 100 22];

            % Create AttenuationTxEditFieldLabel
            app.AttenuationTxEditFieldLabel = uilabel(app.Channel1Panel);
            app.AttenuationTxEditFieldLabel.HorizontalAlignment = 'right';
            app.AttenuationTxEditFieldLabel.Position = [56 56 83 22];
            app.AttenuationTxEditFieldLabel.Text = 'Attenuation Tx';

            % Create AttenuationTx1
            app.AttenuationTx1 = uieditfield(app.Channel1Panel, 'numeric');
            app.AttenuationTx1.ValueChangedFcn = createCallbackFcn(app, @AttenuationTx1ValueChanged, true);
            app.AttenuationTx1.Position = [154 56 100 22];

            % Create AttenuationRxEditFieldLabel
            app.AttenuationRxEditFieldLabel = uilabel(app.Channel1Panel);
            app.AttenuationRxEditFieldLabel.HorizontalAlignment = 'right';
            app.AttenuationRxEditFieldLabel.Position = [56 21 84 22];
            app.AttenuationRxEditFieldLabel.Text = 'Attenuation Rx';

            % Create AttenuationRx1
            app.AttenuationRx1 = uieditfield(app.Channel1Panel, 'numeric');
            app.AttenuationRx1.ValueChangedFcn = createCallbackFcn(app, @AttenuationRx1ValueChanged, true);
            app.AttenuationRx1.Position = [155 21 100 22];

            % Create c1ind
            app.c1ind = uilamp(app.Channel1Panel);
            app.c1ind.Position = [266 98 13 13];

            % Create t1ind
            app.t1ind = uilamp(app.Channel1Panel);
            app.t1ind.Position = [266 61 13 13];

            % Create r1ind
            app.r1ind = uilamp(app.Channel1Panel);
            app.r1ind.Position = [266 25 13 13];

            % Create Channel2Panel
            app.Channel2Panel = uipanel(app.UIFigure);
            app.Channel2Panel.Title = 'Channel 2';
            app.Channel2Panel.BackgroundColor = [1 1 1];
            app.Channel2Panel.FontWeight = 'bold';
            app.Channel2Panel.FontSize = 16;
            app.Channel2Panel.Position = [325 185 299 162];

            % Create CentreFrequencyHzLabel_2
            app.CentreFrequencyHzLabel_2 = uilabel(app.Channel2Panel);
            app.CentreFrequencyHzLabel_2.HorizontalAlignment = 'right';
            app.CentreFrequencyHzLabel_2.Position = [12 94 128 22];
            app.CentreFrequencyHzLabel_2.Text = 'Centre Frequency (Hz)';

            % Create CentreFrequency2
            app.CentreFrequency2 = uieditfield(app.Channel2Panel, 'numeric');
            app.CentreFrequency2.ValueChangedFcn = createCallbackFcn(app, @CentreFrequency2ValueChanged, true);
            app.CentreFrequency2.Tooltip = {'Centre Frequency must be between 2-17.825GHz'};
            app.CentreFrequency2.Position = [155 94 100 22];

            % Create AttenuationTxEditField_2Label
            app.AttenuationTxEditField_2Label = uilabel(app.Channel2Panel);
            app.AttenuationTxEditField_2Label.HorizontalAlignment = 'right';
            app.AttenuationTxEditField_2Label.Position = [57 59 83 22];
            app.AttenuationTxEditField_2Label.Text = 'Attenuation Tx';

            % Create AttenuationTx2
            app.AttenuationTx2 = uieditfield(app.Channel2Panel, 'numeric');
            app.AttenuationTx2.ValueChangedFcn = createCallbackFcn(app, @AttenuationTx2ValueChanged, true);
            app.AttenuationTx2.Position = [155 59 100 22];

            % Create AttenuationRxEditField_2Label
            app.AttenuationRxEditField_2Label = uilabel(app.Channel2Panel);
            app.AttenuationRxEditField_2Label.HorizontalAlignment = 'right';
            app.AttenuationRxEditField_2Label.Position = [56 24 84 22];
            app.AttenuationRxEditField_2Label.Text = 'Attenuation Rx';

            % Create AttenuationRx2
            app.AttenuationRx2 = uieditfield(app.Channel2Panel, 'numeric');
            app.AttenuationRx2.ValueChangedFcn = createCallbackFcn(app, @AttenuationRx2ValueChanged, true);
            app.AttenuationRx2.Position = [155 24 100 22];

            % Create c2ind
            app.c2ind = uilamp(app.Channel2Panel);
            app.c2ind.Position = [266 99 13 13];

            % Create t2ind
            app.t2ind = uilamp(app.Channel2Panel);
            app.t2ind.Position = [266 64 13 13];

            % Create r2ind
            app.r2ind = uilamp(app.Channel2Panel);
            app.r2ind.Position = [266 29 13 13];

            % Create Channel3Panel
            app.Channel3Panel = uipanel(app.UIFigure);
            app.Channel3Panel.Title = 'Channel 3';
            app.Channel3Panel.BackgroundColor = [1 1 1];
            app.Channel3Panel.FontWeight = 'bold';
            app.Channel3Panel.FontSize = 16;
            app.Channel3Panel.Position = [325 14 299 162];

            % Create CentreFrequencyHzLabel_3
            app.CentreFrequencyHzLabel_3 = uilabel(app.Channel3Panel);
            app.CentreFrequencyHzLabel_3.HorizontalAlignment = 'right';
            app.CentreFrequencyHzLabel_3.Position = [11 94 128 22];
            app.CentreFrequencyHzLabel_3.Text = 'Centre Frequency (Hz)';

            % Create CentreFrequency3
            app.CentreFrequency3 = uieditfield(app.Channel3Panel, 'numeric');
            app.CentreFrequency3.ValueChangedFcn = createCallbackFcn(app, @CentreFrequency3ValueChanged, true);
            app.CentreFrequency3.Tooltip = {'Centre Frequency must be between 2-17.825GHz'};
            app.CentreFrequency3.Position = [154 94 100 22];

            % Create AttenuationTxEditField_3Label
            app.AttenuationTxEditField_3Label = uilabel(app.Channel3Panel);
            app.AttenuationTxEditField_3Label.HorizontalAlignment = 'right';
            app.AttenuationTxEditField_3Label.Position = [56 58 83 22];
            app.AttenuationTxEditField_3Label.Text = 'Attenuation Tx';

            % Create AttenuationTx3
            app.AttenuationTx3 = uieditfield(app.Channel3Panel, 'numeric');
            app.AttenuationTx3.ValueChangedFcn = createCallbackFcn(app, @AttenuationTx3ValueChanged, true);
            app.AttenuationTx3.Position = [154 58 100 22];

            % Create AttenuationRxEditField_3Label
            app.AttenuationRxEditField_3Label = uilabel(app.Channel3Panel);
            app.AttenuationRxEditField_3Label.HorizontalAlignment = 'right';
            app.AttenuationRxEditField_3Label.Position = [55 24 84 22];
            app.AttenuationRxEditField_3Label.Text = 'Attenuation Rx';

            % Create AttenuationRx3
            app.AttenuationRx3 = uieditfield(app.Channel3Panel, 'numeric');
            app.AttenuationRx3.ValueChangedFcn = createCallbackFcn(app, @AttenuationRx3ValueChanged, true);
            app.AttenuationRx3.Position = [154 24 100 22];

            % Create c3ind
            app.c3ind = uilamp(app.Channel3Panel);
            app.c3ind.Position = [265 99 13 13];

            % Create t3ind
            app.t3ind = uilamp(app.Channel3Panel);
            app.t3ind.Position = [265 64 13 13];

            % Create r3ind
            app.r3ind = uilamp(app.Channel3Panel);
            app.r3ind.Position = [265 29 13 13];

            % Create Image
            app.Image = uiimage(app.UIFigure);
            app.Image.Position = [6 440 87 81];
            app.Image.ImageSource = 'logo1_name_trans.png';

            % Create RFUpDownConverterLabel
            app.RFUpDownConverterLabel = uilabel(app.UIFigure);
            app.RFUpDownConverterLabel.FontSize = 18;
            app.RFUpDownConverterLabel.FontWeight = 'bold';
            app.RFUpDownConverterLabel.Position = [101 467 204 22];
            app.RFUpDownConverterLabel.Text = 'RF Up Down Converter';

            % Create DDSHzLabel
            app.DDSHzLabel = uilabel(app.UIFigure);
            app.DDSHzLabel.HorizontalAlignment = 'right';
            app.DDSHzLabel.Position = [117 357 57 22];
            app.DDSHzLabel.Text = 'DDS (Hz)';

            % Create DDS
            app.DDS = uieditfield(app.UIFigure, 'numeric');
            app.DDS.ValueChangedFcn = createCallbackFcn(app, @DDSValueChanged, true);
            app.DDS.Tooltip = {'Frequency must be between 500-850MHz'};
            app.DDS.Position = [189 357 100 22];

            % Create DDSIndicator
            app.DDSIndicator = uilamp(app.UIFigure);
            app.DDSIndicator.Position = [296 361 13 13];
            app.DDSIndicator.Color = [1 0 0];

            % Create ResetButton
            app.ResetButton = uibutton(app.UIFigure, 'push');
            app.ResetButton.ButtonPushedFcn = createCallbackFcn(app, @ResetButtonValueChanged, true);
            app.ResetButton.Position = [218 396 71 22];
            app.ResetButton.Text = 'Reset';

            % Show the figure after all components are created
            app.UIFigure.Visible = 'on';
        end
    end

    % App creation and deletion
    methods (Access = public)

        % Construct app
        function app = RFUDCtoolsApp

            % Create UIFigure and components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.UIFigure)

            % Execute the startup function
            runStartupFcn(app, @startupFcn)

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.UIFigure)
        end
    end
end