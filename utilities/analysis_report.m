%% RFUDC V1.0 Report
% DESCRIPTIVE TEXT (very descriptive)

directory = "Results/";
figdirectory = "Figures/";
dat = dir(directory);
dat = dat(12:end);
a = []; 
b1Lin = a;b2Lin = a;b3Lin = a;
b1Settle = a;b2Settle = a;b3Settle = a;
b1SFDR = a;b2SFDR = a;b3SFDR = a;
for i = 1:length(dat)
    temp = split(dat(i).name, '_');
    if(length(temp) == 3)
        a{i}.test = temp{1};
        a{i}.board = temp{2}(end);
        temp = split(temp{3},'.');
        a{i}.centre = temp{1};
        
        temp = load(directory+dat(i).name);
        
        if(a{i}.test == "SettlingTime")
           switch(str2num(a{i}.board))
               case 1
                   b1Settle{end+1,2} = min(temp.settleTimes);
                   b1Settle{end,3} = mean(temp.settleTimes);
                   b1Settle{end,4} = max(temp.settleTimes);
                   b1Settle{end,1} = str2num(a{i}.centre);
               case 2
                   b2Settle{end+1,2} = min(temp.settleTimes);
                   b2Settle{end,3} = mean(temp.settleTimes);
                   b2Settle{end,4} = max(temp.settleTimes);
                   b2Settle{end,1} = str2num(a{i}.centre);
               case 3
                   b3Settle{end+1,2} = min(temp.settleTimes);
                   b3Settle{end,3} = mean(temp.settleTimes);
                   b3Settle{end,4} = max(temp.settleTimes);
                   b3Settle{end,1} = str2num(a{i}.centre);
               otherwise
                   return;
           end
        elseif(a{i}.test == "SFDR")
            switch(str2num(a{i}.board))
                case 1
                    b1SFDR{end+1,2} = round(temp.fundamental.freq/1e6, 3);
                    b1SFDR{end,3} = round(temp.fundamental.amplitude, 2);
                    b1SFDR{end,4} = round((temp.fine.freq - temp.fundamental.freq)/1e3,3);
                    b1SFDR{end,5} = round(temp.fine.sfdr, 2);
                    b1SFDR{end,6} = round((temp.coarse.freq - temp.fundamental.freq)/1e6, 3);
                    b1SFDR{end,7} = round(temp.coarse.sfdr,2);
                    b1SFDR{end,1} = str2num(a{i}.centre);
                case 2
                    b2SFDR{end+1,2} = round(temp.fundamental.freq/1e6, 3);
                    b2SFDR{end,3} = round(temp.fundamental.amplitude, 2);
                    b2SFDR{end,4} = round((temp.fine.freq - temp.fundamental.freq)/1e3,3);
                    b2SFDR{end,5} = round(temp.fine.sfdr, 2);
                    b2SFDR{end,6} = round((temp.coarse.freq - temp.fundamental.freq)/1e6, 3);
                    b2SFDR{end,7} = round(temp.coarse.sfdr,2);
                    b2SFDR{end,1} = str2num(a{i}.centre);
                case 3
                    b3SFDR{end+1,2} = round(temp.fundamental.freq/1e6, 3);
                    b3SFDR{end,3} = round(temp.fundamental.amplitude, 2);
                    b3SFDR{end,4} = round((temp.fine.freq - temp.fundamental.freq)/1e3,3);
                    b3SFDR{end,5} = round(temp.fine.sfdr, 2);
                    b3SFDR{end,6} = round((temp.coarse.freq - temp.fundamental.freq)/1e6, 3);
                    b3SFDR{end,7} = round(temp.coarse.sfdr,2);
                    b3SFDR{end,1} = str2num(a{i}.centre);
                otherwise
                    return;
            end
        elseif(a{i}.test == "Linearity")
            switch(str2num(a{i}.board))
                case 1
                    b1Lin{end+1, 2} = temp.m;
                    b1Lin{end, 3} = temp.dev;
                    b1Lin{end, 1} = str2num(a{i}.centre);
                case 2
                    b2Lin{end+1, 2} = temp.m;
                    b2Lin{end, 3} = temp.dev;
                    b2Lin{end, 1} = str2num(a{i}.centre);
                case 3
                    b3Lin{end+1, 2} = temp.m;
                    b3Lin{end, 3} = temp.dev;
                    b3Lin{end, 1} = str2num(a{i}.centre);
                otherwise
                    return;
            end
        else
            return;
        end
        
    end
end

%% Linearity
% DESCRIPTIVE TEXT

%% Linearity Board 1
h = ["Mean", "Max Deviation", "Centre Frequency (GHz)"];

%% Linearity Board 2
figure('Position', [0 0 445 223]);
h = ["Centre Frequency (GHz)","Mean", "Max Deviation"];
for i = 1:length(b2Lin)
    if(b2Lin{i,3} > 0.010)
        b2Lin{i,4} = 'FAIL';
    else
        b2Lin{i,4} = 'PASS';
    end
end
uitable('Data', b2Lin, 'ColumnName', h, 'Position', [20 20 425 193]);
%%
% Board 2 Average
ave(1) = mean(cell2mat(b2Lin(:,2)));
ave(2) = mean(cell2mat(b2Lin(:,3)));
fprintf("The average linearity is %1.3f and the average deviation for Board 2 is %1.3f\n", ave(1), ave(2));
if(ave(2) < 0.010)
    fprintf("Board 2 PASSES\n");
else
    fprintf("Board 2 FAILS\n");
end
%%
% Maximum Average Linearity
[~, idx] = max(cell2mat(b2Lin(:,2)));
figname = figdirectory + "Linearity_Brd2_" + string(b2Lin(idx,1)) + ".fig";
open(figname);
%%
% Highest Deviation
[~, idx] = max(cell2mat(b2Lin(:,2)));
figname = figdirectory + "Linearity_Brd2_" + string(b2Lin(idx,1)) + ".fig";
open(figname);

%% Linearity Board 3
figure('Position', [0 0 445 223]);
h = ["Centre Frequency (GHz)","Mean", "Max Deviation"];
for i = 1:length(b3Lin)
    if(b3Lin{i,3} > 0.010)
        b3Lin{i,4} = 'FAIL';
    else
        b3Lin{i,4} = 'PASS';
    end
end
uitable('Data', b3Lin, 'ColumnName', h, 'Position', [20 20 425 193]);
%%
% Board 3 Average
ave(1) = mean(cell2mat(b3Lin(:,2)));
ave(2) = mean(cell2mat(b3Lin(:,3)));
fprintf("The average linearity is %1.3f and the average deviation for Board 3 is %1.3f\n", ave(1), ave(2));
if(ave(2) < 0.010)
    fprintf("Board 3 PASSES\n");
else
    fprintf("Board 3 FAILS\n");
end
%%
% Maximum Average Linearity
[~, idx] = max(cell2mat(b3Lin(:,2)));
figname = figdirectory + "Linearity_Brd2_" + string(b3Lin(idx,1)) + ".fig";
open(figname);
%%
% Highest Deviation
[~, idx] = max(cell2mat(b3Lin(:,2)));
figname = figdirectory + "Linearity_Brd2_" + string(b3Lin(idx,1)) + ".fig";
open(figname);

%% SFDR
% DESCRIPTIVE TEXT
%% SFDR Board 1
figure('Position', [0 0 1093 223]);
h = ["Centre Frequency (GHz)","Fundamental Frequency (MHz)", "Fundamental Amplitude (dBm)", ...
    "Fine Spur (kHz)", "Fine SFDR (dB)","Coarse Spur (MHz)", "Coarse SFDR (dB)"];
for i = 1:length(b1SFDR)
    if(b1SFDR{i,5} < 50 || b1SFDR{i,7} < 50)
        b1SFDR{i,8} = 'FAIL';
    else
        b1SFDR{i,8} = 'PASS';
    end
end
uitable('Data', b1SFDR, 'ColumnName', h, 'Position', [20 20 1073 193]);
%%
% Board 1 Average
ave(1) = mean(cell2mat(b1SFDR(:,5)));
ave(2) = mean(cell2mat(b1SFDR(:,7)));
fprintf("The average fine SFDR is %1.3f dB and the average coarse SFDR for Board 1 is %1.3f dB\n", ave(1), ave(2));
if(ave(1) > 50 && ave(2) > 50)
    fprintf("Board 1 PASSES\n");
else
    fprintf("Board 1 FAILS\n");
end
%%
% Worst SFDR
[~, idx] = max(cell2mat(b1SFDR(:,7)));
figname = figdirectory + "SFDR_Brd1_" + string(b1SFDR(idx,1)) + ".fig";
open(figname);
%%
% Best SFDR
[~, idx] = min(cell2mat(b1SFDR(:,7)));
figname = figdirectory + "SFDR_Brd1_" + string(b1SFDR(idx,1)) + ".fig";
open(figname);

%% SFDR Board 2
figure('Position', [0 0 1093 223]);
h = ["Centre Frequency (GHz)","Fundamental Frequency (MHz)", "Fundamental Amplitude (dBm)", ...
    "Fine Spur (kHz)", "Fine SFDR (dB)","Coarse Spur (MHz)", "Coarse SFDR (dB)"];
for i = 1:length(b2SFDR)
    if(b2SFDR{i,5} < 50 || b2SFDR{i,7} < 50)
        b2SFDR{i,8} = 'FAIL';
    else
        b2SFDR{i,8} = 'PASS';
    end
end
uitable('Data', b2SFDR, 'ColumnName', h, 'Position', [20 20 1073 193]);
%%
% Board 2 Average
ave(1) = mean(cell2mat(b2SFDR(:,5)));
ave(2) = mean(cell2mat(b2SFDR(:,7)));
fprintf("The average fine SFDR is %1.3f dB and the average coarse SFDR for Board 2 is %1.3f dB\n", ave(1), ave(2));
if(ave(1) > 50 && ave(2) > 50)
    fprintf("Board 2 PASSES\n");
else
    fprintf("Board 2 FAILS\n");
end
%%
% Worst SFDR
[~, idx] = max(cell2mat(b2SFDR(:,7)));
figname = figdirectory + "SFDR_Brd2_" + string(b2SFDR(idx,1)) + ".fig";
open(figname);
%%
% Best SFDR
[~, idx] = min(cell2mat(b2SFDR(:,7)));
figname = figdirectory + "SFDR_Brd2_" + string(b2SFDR(idx,1)) + ".fig";
open(figname);

%% SFDR Board 3
figure('Position', [0 0 1093 223]);
h = ["Centre Frequency (GHz)","Fundamental Frequency (MHz)", "Fundamental Amplitude (dBm)", ...
    "Fine Spur (kHz)", "Fine SFDR (dB)","Coarse Spur (MHz)", "Coarse SFDR (dB)"];
for i = 1:length(b3SFDR)
    if(b3SFDR{i,5} < 50 || b3SFDR{i,7} < 50)
        b3SFDR{i,8} = 'FAIL';
    else
        b3SFDR{i,8} = 'PASS';
    end
end
uitable('Data', b3SFDR, 'ColumnName', h, 'Position', [20 20 1073 193]);
%%
% Board 1 Average
ave(1) = mean(cell2mat(b3SFDR(:,5)));
ave(2) = mean(cell2mat(b3SFDR(:,7)));
fprintf("The average fine SFDR is %1.3f dB and the average coarse SFDR for Board 3 is %1.3f dB\n", ave(1), ave(2));
if(ave(1) > 50 && ave(2) > 50)
    fprintf("Board 3 PASSES\n");
else
    fprintf("Board 3 FAILS\n");
end
%%
% Worst SFDR
[~, idx] = max(cell2mat(b3SFDR(:,7)));
figname = figdirectory + "SFDR_Brd3_" + string(b3SFDR(idx,1)) + ".fig";
open(figname);
%%
% Best SFDR
[~, idx] = min(cell2mat(b3SFDR(:,7)));
figname = figdirectory + "SFDR_Brd3_" + string(b3SFDR(idx,1)) + ".fig";
open(figname);

%% Settling Time
% DESCRIPTIVE TEXT
%% Settling Time Board 1
figure('Position', [0 0 779 223]);
h = ["Centre Frequency (GHz)","Minimum Settling Time (ns)", ...
    "Mean Settling Time (ns)", "Maximum Settling Time (ns)"];
for i = 1:length(b1Settle)
    if(b1Settle{i,4} > 1040)
        b1Settle{i,5} = 'FAIL';
    elseif(b1Settle{i,4} > 140)
        b1Settle{i,5} = 'SpecFAIL';
    else
        b1Settle{i,5} = 'PASS';
    end
end
uitable('Data', b1Settle, 'ColumnName', h, 'Position', [20 20 759 193]);
%%
% Board 1 Average
ave(1) = mean(cell2mat(b1Settle(:,2)));
ave(2) = mean(cell2mat(b1Settle(:,3)));
ave(3) = mean(cell2mat(b1Settle(:,4)));
fprintf("The average minimum settling time is %3.0f ns \n", ave(1));
fprintf("The average settling time is %3.0f ns \n", ave(2));
fprintf("The average maximum settling time is %3.0f ns \n", ave(3));
if(ave(3) > 1000)
    fprintf("Board 1 FAILS\n");
elseif(ave(3) > 100)
    fprintf("Board 1 FAILS Specification but PASSES requirement\n");
else
    fprintf("Board 1 PASSES\n");
end
%%
% Worst Settling Time
[~, idx] = max(cell2mat(b1Settle(:,3)));
figname = figdirectory + "SettlingTime_Brd1_" + string(b1Settle(idx,1)) + ".fig";
open(figname);
%%
% Best Settling Time
[~, idx] = min(cell2mat(b1Settle(:,3)));
figname = figdirectory + "SettlingTime_Brd1_" + string(b1Settle(idx,1)) + ".fig";
open(figname);

%% Settling Time Board 2
figure('Position', [0 0 779 223]);
h = ["Centre Frequency (GHz)","Minimum Settling Time (ns)", ...
    "Mean Settling Time (ns)", "Maximum Settling Time (ns)"];
for i = 1:length(b2Settle)
    if(b2Settle{i,4} > 1040)
        b2Settle{i,5} = 'FAIL';
    elseif(b2Settle{i,4} > 140)
        b2Settle{i,5} = 'SpecFAIL';
    else
        b2Settle{i,5} = 'PASS';
    end
end
uitable('Data', b2Settle, 'ColumnName', h, 'Position', [20 20 759 193]);
%%
% Board 2 Average
ave(1) = mean(cell2mat(b2Settle(:,2)));
ave(2) = mean(cell2mat(b2Settle(:,3)));
ave(3) = mean(cell2mat(b2Settle(:,4)));
fprintf("The average minimum settling time is %3.0f ns \n", ave(1));
fprintf("The average settling time is %3.0f ns \n", ave(2));
fprintf("The average maximum settling time is %3.0f ns \n", ave(3));
if(ave(3) > 1000)
    fprintf("Board 2 FAILS\n");
elseif(ave(3) > 100)
    fprintf("Board 2 FAILS Specification but PASSES requirement\n");
else
    fprintf("Board 2 PASSES\n");
end
%%
% Worst Settling Time
[~, idx] = max(cell2mat(b2Settle(:,3)));
figname = figdirectory + "SettlingTime_Brd2_" + string(b2Settle(idx,1)) + ".fig";
open(figname);
%%
% Best Settling Time
[~, idx] = min(cell2mat(b2Settle(:,3)));
figname = figdirectory + "SettlingTime_Brd2_" + string(b2Settle(idx,1)) + ".fig";
open(figname);

%% Settling Time Board 3
figure('Position', [0 0 779 223]);
h = ["Centre Frequency (GHz)","Minimum Settling Time (ns)", ...
    "Mean Settling Time (ns)", "Maximum Settling Time (ns)"];
for i = 1:length(b3Settle)
    if(b3Settle{i,4} > 1040)
        b3Settle{i,5} = 'FAIL';
    elseif(b3Settle{i,4} > 140)
        b3Settle{i,5} = 'SpecFAIL';
    else
        b3Settle{i,5} = 'PASS';
    end
end
uitable('Data', b3Settle, 'ColumnName', h, 'Position', [20 20 759 193]);
%%
% Board 3 Average
ave(1) = mean(cell2mat(b3Settle(:,2)));
ave(2) = mean(cell2mat(b3Settle(:,3)));
ave(3) = mean(cell2mat(b3Settle(:,4)));
fprintf("The average minimum settling time is %3.0f ns \n", ave(1));
fprintf("The average settling time is %3.0f ns \n", ave(2));
fprintf("The average maximum settling time is %3.0f ns \n", ave(3));
if(ave(3) > 1000)
    fprintf("Board 3 FAILS\n");
elseif(ave(3) > 100)
    fprintf("Board 3 FAILS Specification but PASSES requirement\n");
else
    fprintf("Board 3 PASSES\n");
end
%%
% Worst Settling Time
[~, idx] = max(cell2mat(b3Settle(:,3)));
figname = figdirectory + "SettlingTime_Brd3_" + string(b3Settle(idx,1)) + ".fig";
open(figname);
%%
% Best Settling Time
[~, idx] = min(cell2mat(b3Settle(:,3)));
figname = figdirectory + "SettlingTime_Brd3_" + string(b3Settle(idx,1)) + ".fig";
open(figname);