function data = GetLog(logName)
    data = extractLogData(strcat(logName, '.dat'));
    data = unstack(data);
end
function data = extractLogData(fileName)
    warning('off');
    dmaLen = 1024;  % Length of each dma output
    % Input logName is <filename>.dat seen in the scope block mask entry
    f = SimulinkRealTime.fileSystem;
    h = fopen(f,fileName);
    data_xpc  = fread(f,h);
    data = SimulinkRealTime.utils.getFileScopeData(data_xpc);
    data = data.data(:,1:dmaLen);
    delete(fileName); % DOUBLE CHECK THIS 
    f.fclose(h);
    warning('on');
end
function data = unstack(rawData)
    [dmaSamp, dmaLen] = size(rawData);
    sigL = dmaSamp*dmaLen;
    rawData = reshape(rawData', 4, sigL/4)';
    sigRaw = reshape(rawData(1:2:end, :)', sigL/2, 1);
    refRaw = reshape(rawData(2:2:end, :)', sigL/2, 1);
    sigBin = dec2bin(sigRaw, 32);
    refBin = dec2bin(refRaw, 32);
    sig(:,1) = typecast(uint16(bin2dec(sigBin(:,17:32))),'int16');
    sig(:,2) = typecast(uint16(bin2dec(sigBin(:,1:16))),'int16');
    ref(:,1) = typecast(uint16(bin2dec(refBin(:,17:32))),'int16');
    ref(:,2) = typecast(uint16(bin2dec(refBin(:,1:16))),'int16');
    sig = reshape(sig', sigL, 1);
    ref = reshape(ref', sigL, 1);
    data.test_signal = double(sig);
    data.reference_signal = double(ref);
    data.time = (0:sigL-1)*1e-9;
end