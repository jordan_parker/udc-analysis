classdef RFUDCtools
    % RFUDCtools are the software tools for operating the RFUDC. 
    % They allow for serial setting of the centre frequency, attenuator 
    % values, and DDS frequencies. This code is necessary for operation of 
    % the RFUDC.
    %
    % RFUDCtools has methods:
    %       - RFUDCtools(port)
    %       - closePort()
    %       - frequency_set(channel, freq, band)
    %       - attenuation_set(attenuation, channel, target)
    %       - dds_table_set(freq, index)
    %       - dds_static_set(freq)
    %       - dds_dynamic_set(freq)
    %
    % Procedures:
    %       1. Define the object. E.g. rfudc = RFUDCtools('COM1');
    %       2. Use the object to set the RFUDC. 
    %       E.g. rfudc.dds_static_set(675e6) OR dds_static_set(rfudc, 675e6);
    %       3. Delete the object after use. E.g. closePort(rfudc);
    %
    % Each setting function contains help documentation for operation.
    %
    % See also CLOSEPORT, FREQUENCY_SET, ATTENUATION_SET,
    % DDS_TABLE_SET, DDS_STATIC_SET, and DDS_DYNAMIC_SET
   properties
      serialport = [];
      config = struct('Frequency', [0,0,0],...
                      'Tx_PLL', [0,0,0],...
                      'Rx_PLL', [0,0,0],...
                      'DDS_Mode', "none",...
                      'Reverse_DDS', [-1, -1, -1],...
                      'Up_Convert_Attenuation', [0,0,0],...
                      'Down_Convert_Attenuation', [0,0,0]);
   end
   properties(Constant)
       bands = struct('A', [1.5, 3],...
           'B', [2.6, 5.1],...
           'C', [4, 6.2],...
           'D', [5.8, 8.4],...
           'E', [7.8, 10],...
           'F', [9.6, 12.4],...
           'G', [11.6, 14.4],...
           'H', [13.2, 16.6],...
           'I', [15.4, 18.3]);
   end
   %% Publicly accessible methods 
   methods(Access = public)
        %% Initialise a serial port 
        function obj = RFUDCtools(port)
           % <strong>RFUDCtools</strong> initialises the RFUDCtools object.
           % <strong>RFUDCtools</strong> has one argument:
           %        - port: string
           %
           %        <strong>port</strong>: is the specified COM port which
           %        the RFUDC is connected to.
           
           % Create a Serial Port
            obj.serialport = serial(port);
            obj.serialport.BaudRate = 9600;
            obj.serialport.Terminator = 'CR';
            
            % Open the serial port
            fopen(obj.serialport);
            disp("RFUDC Initialisation Complete");
        end
        %% Close the serial port 
        function closePort(obj)
          % closePort closes the open serial port. It is a class
          % destructor. It will take the created RFUDCtools object as an
          % input.
          fclose(obj.serialport);
          delete(obj.serialport);
          objname = inputname(1);
          evalin('base', ['clear ' objname]); 
          disp("Closing Port");
        end
        %% Set the RFUDC centre frequency configuration
        function frequency_set(obj, channel, freq, band)
           % frequency_set is a function for setting the centre frequency
           % of the RFUDC at the specified channel.
           %
           % frequency_set has three main arguments:
           %        - channel: int
           %        - freq: int
           %        - band: int
           %        <strong>channel</strong>: is the specified channel i.e., CH (1 - 3).
           %
           %        <strong>freq</strong>: is the centre frequency to be set in Hz. Checks
           %        are in place to ensure the input frequency is correctly
           %        bounded and that incompatible frequencies are not set.
           %        Centre frequencies must be between 1.5 and 18.3 GHz.
           %
           %        <strong>band</strong>: is the specified operational band i.e.,(1 - 9) or (A - I).
           
           if(isa(band, 'char') || isa(band, 'string'))
              band = double(upper(band)) - 64; 
           end
           [valid, errorCode] = frequencyValidCheck(obj, band, freq, channel);
           if(~valid)
               fprintf(errorCode);
               error('User input errors. See above log.');
           end
           
           % Scale frequency to kHz
           freq = freq/1e3;
           
           cmdfreq = strcat('b:', int2str(freq));
           switch(channel)
               case 1
                   % Only initialise fixed PLLs once, when the respective board centre frequency is set
                   cmd = 'i:3';
%                    fprintf(serialPort, cmd);
                   % Set Channel 1 - Board 3 Centre Frequency
                   cmdfreq = strcat(cmdfreq ,':0:3');
               case 2 
                   % Only initialise fixed PLLs once, when the respective board centre frequency is set
                   cmd = 'i:2';
%                    fprintf(serialPort, cmd);
                   % Set Channel 2 - Board 2 Centre Frequency
                   cmdfreq = strcat(cmdfreq ,':0:2');
               case 3 
                   % Only initialise fixed PLLs once, when the respective board centre frequency is set
                   cmd = 'i:1';
%                    fprintf(serialPort, cmd);
                   % Set Channel 3 - Board 1 Centre Frequency
                   cmdfreq = strcat(cmdfreq ,':0:1');
               otherwise
                   errorCode = strcat(errorCode, "Invalid Channel: ");
                   errorCode = strcat(errorCode, string(channel));
                   errorCode = strcat(errorCode, ". Valid Channels are 1 - 3\n");
                   valid = false;
           end
           % Only update if valid
           if(valid)
%                 fprintf(serialPort, cmdfreq);
                freq = freq*1e3;
                fprintf("Channel %d: Centre Frequency set to %2g GHz\n", channel, freq/1e9);
                evalin('base', [inputname(1) '.config.Frequency(' num2str(channel) ') = ' num2str(freq) ';']);
                evalin('base', [inputname(1) '.config.Rx_PLL(' num2str(channel) ') = ' num2str(freq + 1.575e9) ';']);
                switch(band)
                    case {1, 2, 3}
                        evalin('base', [inputname(1) '.config.Tx_PLL(' num2str(channel) ') = ' num2str(freq + 8.175e9) ';']);
                        evalin('base', [inputname(1) '.config.Reverse_DDS(' num2str(channel) ') = true;']);
                    case {4, 5, 6, 7}
                        evalin('base', [inputname(1) '.config.Tx_PLL(' num2str(channel) ') = ' num2str(freq + 3.825e9) ';']);
                        evalin('base', [inputname(1) '.config.Reverse_DDS(' num2str(channel) ') = false;']);
                    case {8}
                        evalin('base', [inputname(1) '.config.Tx_PLL(' num2str(channel) ') = ' num2str(freq + 3.825e9) ';']);
                        evalin('base', [inputname(1) '.config.Reverse_DDS(' num2str(channel) ') = true;']);
                    otherwise
                        evalin('base', [inputname(1) '.config.Tx_PLL(' num2str(channel) ') = ' num2str(freq - 3.825e9) ';']);
                        evalin('base', [inputname(1) '.config.Reverse_DDS(' num2str(channel) ') = true;']);
                end
           end
        end
        %% Set the attenuation of the output signal
        function attenuation_set(obj,attenuation, channel, target)
              % attenuation_set is a function for setting the attenuation
              % of a specified channel manually.
              %
              % attenuation_set has three main arguments:
              %         - attenuation: double
              %         - channel: int
              %         - target: string
              %
              %         <strong>attenuation</strong>: is the desired attenuation of the
              %         target output channel. Attenuation can be between 0
              %         dB and 15.75 dB.
              %
              %         <strong>channel</strong>: is the specified channel i.e., CH (1 - 3).
              %
              %         <strong>target</strong>: is the desired output target. The desired
              %         attenuation will either be applied to the
              %         transmitter or receiver output depending on user
              %         input. Target can be 't', 'T', 'tx', 'Tx', etc. for
              %         attenuating the transmitter. Target can be 'r',
              %         'R', 'rx', 'Rx', etc. for attenuating the receiver.
              %         All other strings are invalid. 
              
              % Scale attenuation to pure integer - no decimals
              attenuation = floor(attenuation*4);
              %% Perform Error Checks
              valid = true;
              errorCode = "ERROR: \n";
              % Attenuation
              % Cannot be greater than 15.75 dB i.e. integer 63 
              if(attenuation > 63)
                   errorCode = strcat(errorCode, "Attenuation setting greater than 63 (0x3F)\n");
                   valid = false;
              elseif(attenuation < 0)
                   errorCode = strcat(errorCode, "Attenuation setting less than 0 (0x00)\n");
                   valid = false;
              end
              % Channel
              % Can only be between 1 and 3
              if(channel > 3 || channel < 1)
                   errorCode = strcat(errorCode, "Invalid Channel: ");
                   errorCode = strcat(errorCode, string(channel));
                   errorCode = strcat(errorCode, ". Valid Channels are 1 - 3\n");
                   valid = false; 
              end
              % Target
              % Only transmitter and receiver targets
              if(strcmpi(target(1), 't'))
                  tx = 't'; 
              elseif(strcmpi(target(1), 'r'))
                  tx = 'r';
              else
                 errorCode = strcat(errorCode, "Target is invalid. Valid targets are 'tx' or 'rx' \n");
                 valid = false; 
              end
              %% Set attenuation
              if(valid)
                  cmd = strcat("c:", tx, ":", string(attenuation));
                  if(strcmpi(target(1), 't'))
                    fprintf("Setting Attenuation of Tx on ");
                    evalin('base', strcat(inputname(1), '.config.Up_Convert_Attenuation(', num2str(channel),') = ', num2str(attenuation/4), ';'));
                  else
                    fprintf("Setting Attenuation of Rx on ");
                    evalin('base', strcat(inputname(1), '.config.Down_Convert_Attenuation(', num2str(channel),') = ', num2str(attenuation/4), ';'));
                  end
                  switch(channel)
                      case 1
                          cmd = strcat(cmd, ":3");
                      case 2
                          cmd = strcat(cmd, ":2");
                      case 3
                          cmd = strcat(cmd, ":1");
                  end
                  fprintf(serialPort, cmd);
                  fprintf("Board %d to %g dBm (0x%02x)\n", channel, attenuation/4, attenuation);
              else
                  fprintf(errorCode);
              end
        end
        %% Set the default values in the DDS tables
        function dds_table_set(obj, freq, index)
           % dds_table_set is a function for setting the frequencies in the
           % DDS tables.
           %
           % dds_table_set has two main arguments:
           %        - freq: int
           %        - index: int
           %
           %        <strong>freq</strong>: is the DDS frequency desired to be set in Hz.
           %        Valid DDS frequencies are between 500 and 850 MHz.
           %
           %        <strong>index</strong>: is the index of the table entry that is being
           %        updated with a new frequency. Valid indexes are between
           %        0 and 511.
           
           %% Perform Error Checks
           errorCode = "ERROR: \n";
           valid = true;
           % Frequency
           % Must be between 500 and 850 MHz
           if(freq > 850000000)
               errorCode = strcat(errorCode, "Frequency setting greater than 850MHz\n");
               valid = false;
           elseif(freq < 500000000)
               errorCode = strcat(errorCode, "Frequency setting less than 500MHz \n");
               valid = false;
           end
           % Index
           % Indexes must be between 0 and 511
           if(index > 511 || index < 0)
               errorCode = strcat(errorCode, "Invalid Index: ");
               errorCode = strcat(errorCode, string(index));
               errorCode = strcat(errorCode, ". Valid Indexes are 0 - 511. \n");
               valid = false;
           end
           %% Set DDS table entry
           if(valid == true)
               cmd = strcat("f:up:", string(index));
               cmd = strcat(cmd, ":");
               cmd = strcat(cmd, string(freq));
               fprintf(obj.serialport, cmd);
               cmd = strcat("f:down:", string(index));
               cmd = strcat(cmd, ":");
               cmd = strcat(cmd, string(1350000000 - freq));
               fprintf(obj.serialport, cmd);
               fprintf("Setting DDS Table Entry %d to...\n\t UP: %g MHz\n\t DOWN: %g MHz\n", index, freq/1000000, 1350 - freq/1000000);
           else
               fprintf(errorCode);
           end
        end
        %% Set the DDS statically through the serial port - NOT RECOMMENDED
        function dds_static_set(obj, freq)
            % dds_static_set is a function for setting the DDS frequency
            % statically through the serial port. This is NOT RECOMMENDED.
            % Optimal use of the RFUDC utilises the LVDS and trigger line
            % connection. dds_set confirms whether the user wants to
            % statically set the DDS which stops it from being iteratively
            % called.
            %
            % dds_set has one input argument:
            %       - freq: int
            %
            %       <strong>freq</strong>: is the DDS frequency desired to be set in Hz.
            %       Valid DDS frequencies are between 500 and 850 MHz.
            
            %% Error Checks
            errorCode = "ERROR: \n";
            valid = true;
            % Frequency
            % Must be between 500 and 850 MHz
            if(freq > 850e6)
                errorCode = strcat(errorCode, "Frequency setting greater than 850MHz\n");
                valid = false;
            elseif(freq < 500e6)
                errorCode = strcat(errorCode, "Frequency setting less than 500MHz \n");
                valid = false;
            end
            %% Set DDS
            if(valid)
                fprintf(obj.serialport, "t:en");
                cmd = strcat("t:d:", string(freq));
                fprintf(obj.serialport, cmd);
                fprintf("Setting DDS to ... \n\t UP: %g MHz \n\t DOWN: %g MHz \n", freq/1e6, 1350 - freq/1e6);
                evalin('base', strcat(inputname(1), '.config.DDS_Mode = ', sprintf('"static - %d MHz";', freq/1e6)));
            else
                fprintf(errorCode);
            end
        end
        %% Set the DDS through an SLRT
        function dds_dynamic_set(obj)
            % dds_dynamic_set clears the static mode from the RFUDC to
            % allow for agile operation. dds_dynamic_set has no input
            % arguments.
            fprintf(obj.serialport, "t:c");
            evalin('base', strcat(inputname(1), '.config.DDS_Mode = "agile"'));
        end
   end
   %% Private methods used internally | DO NOT ALTER
   methods(Access = private, Hidden = true,Sealed = true)
       %% Check the specified frequency is valid given the full board configuration.
       function [valid, errorCode] = frequencyValidCheck(obj, band, freq, channel)
           valid = true;
           bandValid = true;
           errorCode = "--------------------------------------------------------\n";
           errorCode = strcat(errorCode, '<strong>ERROR:</strong>');
           % Check frequency bounds
           if(freq > 18.3e9)
               errorCode = strcat(errorCode, "\n\tInvalid Centre Frequency. Frequency greater than 18.3 GHz \n");
               valid = false;
           elseif(freq < 1.5e9)
               errorCode = strcat(errorCode, "\n\tInvalid Centre Frequency. Frequency less than 1.5 GHz \n");
               valid = false;
           end
           % Check Band bounds
           if(band < 1 || band > 9)
               errorCode = strcat(errorCode, "\n\tInvalid Band. Supported bands are (1-9) or (A-I)\n");
               valid = false;
               bandValid = false;
           end
           if(bandValid && valid)
               % Valid Band exists but isn't set
               [bandValidStr, bandValid] = bandSearch(obj, freq);
               if(~any(bandValid == band))
                   bandStr = char(band + 64);
                   lower = eval(['obj.bands.' bandStr '(1)']);
                   upper = eval(['obj.bands.' bandStr '(2)']);
                   % Two Valid Bands
                   if(length(bandValid) == 2)
                        errorCode = strcat(errorCode, sprintf(['\n\tInvalid Frequency for Band %s.\n'...
                            '\tBand %s supports frequencies between %g GHz and %g GHz.\n' ...
                            '\tBoth Band %s and Band %s will support a frequency of %g GHz\n'], bandStr, bandStr, ...
                            lower, upper, bandValidStr{1}, bandValidStr{2}, freq/1e9));
                   % One Valid Band
                   else
                        errorCode = strcat(errorCode, sprintf(['\n\tInvalid Frequency for Band %s.\n'...
                            '\tBand %s supports frequencies between %g GHz and %g GHz.\n' ...
                            '\tBand %s will support a frequency of %g GHz\n'], bandStr, bandStr, ...
                            lower, upper, bandValidStr{1}, freq/1e9));
                   end
                   valid = false;
               end
           end
           if(channel < 1 || channel > 3)
               errorCode = strcat(errorCode, '\n\tInvalid Channel. Channels must be 1-3 \n');
               valid = false;
           end
           if(valid && ~strcmpi(obj.config.DDS_mode, 'agile'))
               flipValid = false(1,3);
               for i = 1:3
                   if(i == channel)
                       continue;
                   elseif((band < 8 && band > 3) ~= obj.config.Reverse_DDS(i)) 
                        flipValid = [flipValid true];
                   end
               end
               fprintf('The current setting will disrupt frequency agility operation in Board %d \n', find(flipValid));
               yn = input('Do you wish to continue? y/n?');
               if(strcmp(yn(1), 'y'))
                   obj.config.Reverse_DDS(flipValid) = -1;
               else
                   valid = false;
                   errorCode = strcat(errorCode,'\n\tIncompatible DDS configuration');
                   return;
               end
           end
           errorCode = strcat(errorCode, "--------------------------------------------------------\n");
       end
       %% Find the bands corresponding to a given frequency.
       function [validStr, validNum] = bandSearch(obj, freq)
           % Check which band the frequency is in
           bandValid = false(1,9);
           freq = freq/1e9; % Convert to GHz
           for i = 1:9
               bandStr = char(i + 64);
               lower = eval(['obj.bands.' bandStr '(1)']);
               upper = eval(['obj.bands.' bandStr '(2)']);
               if(freq < lower || freq > upper)
                   bandValid(i) = false;
               else
                   bandValid(i) = true;
               end
               % Stop if valid band/s found
               if(i > 1 && bandValid(i-1))
                   break;
               end
           end
           % Find idxs for valid bands
           validNum = find(bandValid);
           validStr = {};
           % Convert to character bands
           for j = 1:length(validNum)
               validStr{j} = char(validNum(j)+64);
           end
       end
   end
end