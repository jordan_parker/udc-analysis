%% ###############################################
%                   Procedures
%   Running the test:
%       1. Alter the execution flags under properties to your desired
%       configuration. E.g. run_execution = true;
%       2. If using an RFUDC, enter the correct COM port in RFUDC_comport.
%       3. Run the desired test using runtests('Test_Settling_Time').
%
%   runtests('Test_Settling_Time') => All tests
%   runtests('Test_Settling_Time','ParameterName', '[TestParameter]') => 
%   Runs only tests that contain the specified parameter.
%   runtests('Test_Settling_Time','Name', '[TestName]') => Runs a specific 
%   test.
%
%   Developer Notes:
%       1. Default properties and test parameters can be altered to
%       increase or alter test functionality. E.g. SwitchTime can be 
%       extended to run for a wider range of DDS switching periods.
%  ###############################################
classdef Test_Settling_Time < matlab.unittest.TestCase
    properties(Constant)
        baseDir = fileparts(fileparts(mfilename('fullpath')));
        Fs = 1e9;
    end
    properties
        %% Execution Flags
        load_model = false; % Loads the model to the default Speedgoat
        run_execution = false; % Runs execution on the Speedgoat
        run_analysis = true; % Runs the test analysis
        run_figures = true; % Generates figures for the analysis
        run_report = true; % Generates a summary report
        %% RFUDC
        RFUDC_comport = '';
        RFUDC = [];
        %% Speedgoat
        tg = [];
        %% Default Parameters
        StopTime = 1;
        gain = 2^14-1;
        trigger_width = 3;
        Center_IF = 250e6;
    end
    properties(TestParameter)
        SwitchTime = struct('ns_1000', 1,...
            'ns_500', 0.5,...
            'ns_100', 0.1);
    end
   %% Functions to run before the test
   methods(TestClassSetup)
       function loadModel(self)
           if(self.run_execution)
               self.tg = slrt
               if(self.load_model)
                   % Load Model
                   fprintf('\tLoading Model to default Speedgoat ...');
                   self.tg.load('Analysis_HDL');
                   disp('Done');
               end
               % Set model defaults
               self.tg.StopTime = self.StopTime;
               if(self.tg.SampleTime ~= 1e-3)
                   warning('Sample Time is not 1ms. A rebuild may be necessary.');
                   self.tg.SampleTime = 1e-3;
               end
               self.tg.setparam('trigger_width', self.trigger_width);
               self.tg.setparam('gain_control', self.gain);
               self.tg.setparam('selection_type', 1);
               self.tg.setparam('dma_enable', 0);
               self.tg.setparam('channel_select', floor(self.Center_IF/10e6) - 10);
           end
       end
       function setupRFUDC(self)
           if(self.run_execution)
               if(~isempty(self.RFUDC_comport))
                   self.RFUDC = RFUDCtools(self.RFUDC_comport);
                   % Configure agile DDS
                   dds_dynamic_set(self.RFUDC);
                   % Default Band - PLLs don't matter for switching time
                   frequency_set(self.RFUDC, 1, 2e9, 'A');
               end
           end
       end
   end
   %% The main test function(s)
   methods(Test)
       function TestSettlingTime(self, SwitchTime)
           fprintf('################################################################\n');
           fprintf('\tStarting Settling Time Test: Switching Period - %d ns\n', SwitchTime*1e3);
           fprintf('----------------------------------------------------------------\n');
           %% Execute the test
           if(self.run_execution)
               self.tg.setparam('switching_period_us', SwitchTime);
               self.executeTest(SwitchTime);
           end
           %% Analysis
           if(self.run_analysis)
               self.TimeAnalysis(SwitchTime, 2^10, 0);
           end
       end
   end
   %% Teardown methods
   methods(TestClassTeardown)
       function tdRFUDC(self)
          if(~isempty(self.RFUDC))
            closePort(self.RFUDC);
          end
       end
       function summaryReport(self)
           %% Report
           if(self.run_report)
               % Summary Report Generation
           end
       end
   end
   %% Extra functions
   methods(Access = public)
       function executeTest(self, SwitchTime)
           self.tg.start;
           self.tg.setparam('dma_enable', 1);
           while(strcmpi(self.tg.status, 'running'))
           end
           data = GetLog('DMAdata');
           save(fullfile(self.baseDir, 'results', 'Settle_Time', 'Data', sprintf('TestSettle_%gns', SwitchTime*1e3)), 'data');
       end
       function TimeAnalysis(self, SwitchTime, FFTlen, BW)
           % Load data
           load(fullfile(self.baseDir, 'results', 'Settle_Time', 'Data', sprintf('TestSettle_%gns', SwitchTime*1e3)));
           dec = 1;
           N = length(data.test_signal);
            % Scale switch speed to ns
            swSpeed = SwitchTime*1e3;
            pks = zeros(floor(N/(2*swSpeed)),2);
            trghs = zeros(floor(N/(2*swSpeed)),2);
            settleTimes = zeros(floor(N/(2*swSpeed)),1);
            swSpeed = swSpeed/2^dec;
            tgtAmp = zeros(N/2,1);              % Array of amplitudes of the target frequency at each FFT frame
    %         tgtIdx = tgtFreq*FFTlen/self.Fs;                 % FFT index corresponding to the target frequency
            BWIdx = BW*FFTlen/self.Fs;                       % FFT index associated with the bandwidth around the target frequency 

            % Determine fundamental frequency index
            [~,tgtIdx] = max(analysisTools.freqAnalysis(data.reference_signal, FFTlen, 2));

            % Frequency analysis to track amplitude of target frequency
            fprintf("\t Starting Frequency Analysis ... ");
            sig = data.test_signal/2^16;
            % Window Definition
            w = hann(FFTlen/2, 'periodic');
            w(1:FFTlen/2^2) = 1;
            w(end-FFTlen/2^2:end) = 1;
            w(1:FFTlen/2^8) = 0;
            w(end-FFTlen/2^8:end) = 0;
            
            % FFT per timestep
            for t = 1:(N/2)
                % Condition FFT
                FFT = fft(sig(t:t+FFTlen-1));
                FFT = abs(FFT/FFTlen);
                % One Sided
                FFT = FFT(FFTlen/2+1:FFTlen);                    
                FFT(2:end-1) = 2*FFT(2:end-1);
                FFT = w.*FFT;
                % Maximum amplitude of primary frequency
                [~, maxIdx] = max(FFT);
                tgtAmp(t) = max(FFT(round(tgtIdx-BWIdx):round(tgtIdx+BWIdx)));
            end
            disp("Finished");
            
            fprintf("\t Smoothing Amplitude Reponse ... ");
            % Average amplitude response of target frequency
            tgtAmp = analysisTools.conversiondBm(tgtAmp);

            temp = [];
            % Moving average for target amplitude
            for t = 1:2^dec:length(tgtAmp)-2^dec
                temp(end+1) = mean(tgtAmp(t:t+2^dec));
            end
            tgtAmp = temp;
            
            % Find the rate of change of target amplitude over time
            slope = diff(tgtAmp(:));
            disp("Finished");
            
            fprintf("\t Finding Settling Time Edges ... ");
            % Find settling time edges
            idx = 1;
            % Find first peak and trough of slope
            [pks(idx, 1), pks(idx, 2)] = max(slope(1:1+swSpeed));
            [trghs(idx, 1), trghs(idx, 2)] = min(slope(1:1+swSpeed));
            % If Peak occurs before trough offset such that unsettled signal is roughly centered
            offset = pks(idx,2) + swSpeed/2;
            % Otherwise don't offset
            if(pks(idx,2) > trghs(idx,2))
                offset = 1;
            end
            for i = offset:swSpeed:length(slope)-swSpeed
               % Find peaks and troughs of slope
               [pks(idx, 1), pks(idx, 2)] = max(slope(i:i+swSpeed));
               [trghs(idx, 1), trghs(idx, 2)] = min(slope(i:i+swSpeed));
               % Find peak and trough indexes
               pks(idx, 2) = pks(idx, 2) + i;
               trghs(idx, 2) = trghs(idx, 2) + i;
               if(pks(idx,2) > trghs(idx,2))
                   % Determine threshold based on surrounding noise
                   thresh = mean(findpeaks(abs(slope(cat(2, i:trghs(idx,2),pks(idx,2):i+swSpeed)))));
                   % If peak is not significant enough, discard it
                   if((pks(idx,1)-trghs(idx,1)) > 7*thresh)
                       idx = idx + 1;
                   end
               end
            end
            
            % Remove trailing zeroes
            pks = pks(1:idx-1,:);
            trghs = trghs(1:idx-1,:);
            pksZero = zeros(idx-1,2);
            trghsZero = zeros(idx-1,2);
            
            % Look at first amplitude interval
            w = tgtAmp(1:trghs(1,2));
            % Find 'zero' based on first peak preceding slope trough
            [~,ttemp] = findpeaks(w);
            trghsZero(1,2) = ttemp(end-1)+1;
            trghsZero(1,1) = tgtAmp(trghsZero(1,2));
            for j = 2:size(pks)
                w = tgtAmp(pks(j-1,2):trghs(j,2));
                [~,ptemp] = findpeaks(w);
                % Find 'zero' based on first peak preceding slope trough
                trghsZero(j,2) = pks(j-1,2) + ptemp(end-1) + 1;
                trghsZero(j,1) = tgtAmp(trghsZero(j,2));
                % Find 'zero' based on first peak after slope peak
                pksZero(j-1,2) = pks(j-1,2) + ptemp(2) - 1;
                pksZero(j-1,1) = tgtAmp(pksZero(j-1,2));
            end
            % Look at last amplitude interval
            w = tgtAmp(pks(end-1):end);
            [~,ptemp] = findpeaks(w);
            % Find 'zero' based on first peak after slope peak
            pksZero(idx-1,2) = pks(idx-1,2) + ptemp(2)-1;
            pksZero(idx-1,1) = tgtAmp(pksZero(idx-1,2));
            
            % Calculate settling time based on zero intervals
            settleTimes(1:idx-1) = (2^dec)*(pksZero(:,2) - trghsZero(:,2))-FFTlen;
    %         settleTimes = (2^dec)*(pksZero((1:idx-1),2) - trghsZero((1:idx-1),2))-FFTlen;
            disp("Finished");
            fprintf('----------------------------------------------------------------\n');
            
            save(fullfile(self.baseDir, 'results', 'Settle_Time', 'Analysis', sprintf('TestSettle_%gns', SwitchTime*1e3)),...
                'settleTimes', 'slope', 'tgtAmp', 'pks', 'trghs', 'pksZero', 'trghsZero');
            if(self.run_figures)
                self.TimeFigures(SwitchTime);
            end
       end
       function TimeFigures(self, SwitchTime)
           % Load Data
           load(fullfile(self.baseDir, 'results', 'Settle_Time', 'Analysis',sprintf('TestSettle_%gns', SwitchTime*1e3)));
           dec = 1;
           time = (1:length(tgtAmp))*(2^dec);
           timeSlope = (1:length(slope))*(2^dec);
           
           % Plot
           f2 = figure();
           f2.Position = [100 100 1000 800];
           
           y = subplot(2,1,1);
           plot(time, tgtAmp, pksZero(:,2)*(2^dec), pksZero(:,1), 'g*', trghsZero(:,2)*(2^dec), trghsZero(:,1), 'm*');
           title('Amplitude of fundamental frequency over time: 250 MHz')
           xlabel('Time (ns)')
           ylabel('Amplitude (dBm)')
           axis([timeSlope(1) timeSlope(end) floor(min(tgtAmp)) ceil(max(tgtAmp))])
           
           dy = subplot(2,1,2);
           plot(timeSlope, slope, pks(:,2)*(2^dec), pks(:,1), 'r*', trghs(:,2)*(2^dec), trghs(:,1), 'b*');
           axis([timeSlope(1) timeSlope(end) 1.2*min(slope) 1.2*max(slope)])
           title('Slope of fundamental frequency over time: 250 MHz')
           xlabel('Time (ns)')
           ylabel('Amplitude per Nanosecond (dBm/ns)')
           xlim([0,timeSlope(end)]);
           linkaxes([y, dy], 'x');
           
           savefig(fullfile(self.baseDir, 'results', 'Settle_Time', 'Figures', sprintf('TestSettle_%gns', SwitchTime*1e3)));
           close all;
       end
   end
end