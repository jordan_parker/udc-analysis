%% ###############################################
%                   Procedures
%   Running the test:
%       1. Alter the execution flags under properties to your desired
%       configuration. E.g. run_execution = true;
%       2. If using an RFUDC, enter the correct COM port in RFUDC_comport.
%       3. Run the desired test using runtests('Test_Linearity').
%
%   runtests('Test_Linearity') => All tests
%   runtests('Test_Linearity','ParameterName', '[TestParameter]') => Runs 
%   only tests that contain the specified parameter.
%   runtests('Test_Linearity','Name', '[TestName]') => Runs a specific test
%
%   Developer Notes:
%       1. Default properties and test parameters can be altered to
%       increase or alter test functionality. E.g. Gains can be extended to
%       run for a wider range of input voltages.
%  ###############################################
classdef Test_Linearity < matlab.unittest.TestCase
    properties(Constant)
        baseDir = fileparts(fileparts(mfilename('fullpath')));
        Fs = 1e9;
    end
    properties
        %% Execution Flags
        load_model = false; % Loads the model to the default Speedgoat
        run_execution = false; % Runs execution on the Speedgoat
        run_analysis = true; % Runs the test analysis
        run_figures = true; % Generates figures for the analysis
        run_report = true; % Generates a summary report
        %% RFUDC
        RFUDC_comport = '';
        RFUDC = [];
        %% Speedgoat
        tg = [];
        %% Default Parameters
        StopTime = 1;
        trigger_width = 3;
        Center_IF = 250e6;
        Gains = [2^14-1,...
                 3*2^12-1,...
                 2^13-1,...
                 3*2^11-1,...
                 2^12-1,...
                 2^11-1,...
                 2^10-1];
    end
    properties(TestParameter)
       Band = struct('A', [2e9;1],...
           'B', [4e9;2],...
           'C', [5e9;3],...
           'D', [7e9;4],...
           'E', [9e9;5],...
           'F', [11e9;6],...
           'G', [13e9;7],...
           'H', [15e9;8],...
           'I', [17e9;9],...
           'Loopback', [0;0]);
        Channel = struct('CH1', 1,...
            'CH2', 2,...
            'CH3', 3);
    end
   %% Functions to run before the test
   methods(TestClassSetup)
       function loadModel(self)
           if(self.run_execution)
               self.tg = slrt
               if(self.load_model)
                   % Load Model
                   fprintf('\tLoading Model to default Speedgoat ...');
                   self.tg.load('Analysis_HDL');
                   disp('Done');
               end
               % Set model defaults
               self.tg.StopTime = self.StopTime;
               if(self.tg.SampleTime ~= 1e-3)
                   warning('Sample Time is not 1ms. A rebuild may be necessary.');
                   self.tg.SampleTime = 1e-3;
               end
               self.tg.setparam('trigger_width', self.trigger_width);
               self.tg.setparam('selection_type', 0);
               self.tg.setparam('dma_enable', 0);
               self.tg.setparam('channel_select', floor(self.Center_IF/10e6) - 10);
           end
       end
       function setupRFUDC(self)
           if(self.run_execution)
               if(~isempty(self.RFUDC_comport))
                   self.RFUDC = RFUDCtools(self.RFUDC_comport);
                   dds_static_set(self.RFUDC, 675e6);
               end
           end
       end
   end
   %% The main test function(s)
   methods(Test)
       function TestLinearity(self, Channel, Band)
           fprintf('################################################################\n');
           fprintf('\tStarting Linearity Test\n');
           if(any(Band))
               fprintf('\tRFUDC CH%d Band %s - Frequency %g GHz \n', Channel, char(Band(2)+64), Band(1)/1e9);
           end
           fprintf('----------------------------------------------------------------\n');
           %% Execute the test
           if(self.run_execution)
               % Set RFUDC
               if(any(Band))
                   frequency_set(self.RFUDC, Channel, Band(1), Band(2));
               end
               self.executeTest(Channel, Band);
           end
           %% Analysis
           if(self.run_analysis)
               self.LinearityAnalysis(Channel, Band);
           end
       end
   end
   %% Teardown methods
   methods(TestClassTeardown)
       function tdRFUDC(self)
           if(~isempty(self.RFUDC))
               closePort(self.RFUDC);
           end
       end
       function summaryReport(self)
           %% Report
           if(self.run_report)
               % Summary Report Generation
           end
       end
   end
   %% Extra functions
   methods(Access = public)
       function executeTest(self, Channel, Band)
           data = cell(length(self.Gains),2);
           for i = 1:length(self.Gains)
               self.tg.setparam('gain_control', self.Gains(i));
               self.tg.start;
               self.tg.setparam('dma_enable', 1);
               while(strcmpi(self.tg.status, 'running'))
               end
               data{i,1} = self.Gains(i);
               data{i,2} = GetLog('DMAdata');
           end
           save(fullfile(self.baseDir, 'results', 'Linearity', 'Data', sprintf('TestLinearity_CH%d_Bnd%d', Channel, Band(2))), 'data');
       end
       function LinearityAnalysis(self, Channel, Band)
           % Load Data
           load(fullfile(self.baseDir, 'results', 'Linearity', 'Data', sprintf('TestLinearity_CH%d_Bnd%d', Channel, Band(2))));
           
           % Find amplitude of signal and reference
           FFTlen = 2^10;
           for i = 1:length(data)
              input_v_output(i,1) = max(analysisTools.freqAnalysis(data{i,2}.test_signal, FFTlen, 2));
              input_v_output(i,2) = max(analysisTools.freqAnalysis(data{i,2}.reference_signal, FFTlen, 2));
           end           
           ratio = input_v_output(:,2)./input_v_output(:,1);
           average = mean(ratio);
           diff = input_v_output(:,2) - average*input_v_output(:,1);
           [~, deviation] = max(abs(diff));
           deviation = diff(deviation);
           save(fullfile(self.baseDir, 'results', 'Linearity', 'Analysis', sprintf('TestLinearity_CH%d_Bnd%d', Channel, Band(2))),...
                'ratio','input_v_output', 'average', 'deviation');
            if(self.run_figures)
                self.LinearityFigures(Channel, Band)
            end
       end
       function LinearityFigures(self, Channel, Band)
           % Load Data
           load(fullfile(self.baseDir, 'results', 'Linearity', 'Analysis', sprintf('TestLinearity_CH%d_Bnd%d', Channel, Band(2))));
           
           % Plot
           figure();
           title('Voltage Input vs Voltage Output')
           xlabel('Voltage Output (V)')
           ylabel('Voltage Input (V)')
           hold on;
           plot(input_v_output(:,2), input_v_output(:,1),'*');
           axis([0 0.25 0 0.25]);
           
           savefig(fullfile(self.baseDir, 'results', 'Linearity', 'Figures', sprintf('TestLinearity_CH%d_Bnd%d', Channel, Band(2))));
           close all;
       end
   end
end