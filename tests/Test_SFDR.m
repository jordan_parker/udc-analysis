%% ###############################################
%                   Procedures
%   Running the test:
%       1. Alter the execution flags under properties to your desired
%       configuration. E.g. run_execution = true;
%       2. If using an RFUDC, enter the correct COM port in RFUDC_comport.
%       3. Run the desired test using runtests('Test_SFDR').
%
%   runtests('Test_SFDR') => All tests
%   runtests('Test_SFDR','ParameterName', '[TestParameter]') => Runs only 
%   tests that contain the specified parameter.
%   runtests('Test_SFDR','Name', '[TestName]') => Runs a specific test.
%
%   Developer Notes:
%       1. Default properties and test parameters can be altered to
%       increase or alter test functionality. E.g. BW can be 
%       extended to run for a wider range of SFDR windows.
%  ###############################################
classdef Test_SFDR < matlab.unittest.TestCase
   properties(Constant)
       baseDir = fileparts(fileparts(mfilename('fullpath')));
       Fs = 1e9;
   end
   properties
       %% Execution Flags
        load_model = false; % Loads the model to the default Speedgoat
        run_execution = false; % Runs execution on the Speedgoat
        run_analysis = true; % Runs the test analysis
        run_figures = true; % Generates figures for the analysis
        run_report = true; % Generates a summary report
       %% RFUDC
       RFUDC_comport = '';
       RFUDC = [];
       %% Speedgoat
       tg = [];
       %% Default Parameters
       StopTime = 1;
       gain = 2^14-1;
       trigger_width = 3;
       Center_IF = 250e6;
   end
   properties(TestParameter)
       FFTlen = struct('Power_18', 2^18);
       BW = struct('MHz_5', 5e6,...
           'MHz_10', 10e6);
       Band = struct('A', [2e9;1],...
           'B', [4e9;2],...
           'C', [5e9;3],...
           'D', [7e9;4],...
           'E', [9e9;5],...
           'F', [11e9;6],...
           'G', [13e9;7],...
           'H', [15e9;8],...
           'I', [17e9;9],...
           'Loopback', [0;0]);
       Channel = struct('CH1', 1,...
           'CH2', 2,...
           'CH3', 3);
   end
   %% Functions to run before the test
   methods(TestClassSetup)
       function loadModel(self)
           if(self.run_execution)
               self.tg = slrt
               if(self.load_model)
                   % Load Model
                   fprintf('\tLoading Model to default Speedgoat ...');
                   self.tg.load('Analysis_HDL');
                   disp('Done');
               end
               % Set model defaults
               self.tg.StopTime = self.StopTime;
               if(self.tg.SampleTime ~= 1e-3)
                   warning('Sample Time is not 1ms. A rebuild may be necessary.');
                   self.tg.SampleTime = 1e-3;
               end
               self.tg.setparam('trigger_width', self.trigger_width);
               self.tg.setparam('gain_control', self.gain);
               self.tg.setparam('selection_type', 0);
               self.tg.setparam('dma_enable', 0);
               self.tg.setparam('channel_select', floor(self.Center_IF/10e6) - 10);
           end
       end
       function setupRFUDC(self)
           if(self.run_execution)
               if(~isempty(self.RFUDC_comport))
                   self.RFUDC = RFUDCtools(self.RFUDC_comport);
                   dds_static_set(self.RFUDC, 675e6);
               end
           end
       end
   end
   %% The main test function(s)
   methods(Test)
       function TestSFDR(self, Channel, Band, BW, FFTlen)
           fprintf('################################################################\n');
           fprintf('\tStarting SFDR Test: FFT Length - %d, Bandwidth - %g MHz \n', FFTlen, BW/1e6);
           if(any(Band))
               fprintf('\tRFUDC CH%d Band %s - Frequency %g GHz \n', Channel, char(Band(2)+64), Band(1)/1e9);
           end
           fprintf('----------------------------------------------------------------\n');
           %% Execute the test
           if(self.run_execution)
               % Set RFUDC
               if(any(Band))
                    frequency_set(self.RFUDC, Channel, Band(1), Band(2));   
               end
               self.executeTest(Channel, Band, BW);
           end
           %% Analysis
           if(self.run_analysis)
               self.SFDRAnalysis(FFTlen, BW, Band, Channel);
           end
       end
   end
   %% Teardown methods
   methods(TestClassTeardown)
       function tdRFUDC(self)
           if(~isempty(self.RFUDC))
               closePort(self.RFUDC);
           end
       end
       function summaryReport(self)
           %% Report
           if(self.run_report)
               % Summary Report Generation
           end
       end
   end
   %% Extra functions
   methods(Access = public)
       function executeTest(self, Channel, Band)
           self.tg.start;
           self.tg.setparam('dma_enable', 1);
           while(strcmpi(self.tg.status, 'running'))
           end
           data = GetLog('DMAdata');
           save(fullfile(self.baseDir, 'results', 'SFDR', 'Data', sprintf('TestSFDR_CH%d_Bnd%d', Channel, Band(2))), 'data');
       end
       function SFDRAnalysis(self, FFTlen, BW, Band, Channel)
            % Load test data 
            load(fullfile(self.baseDir, 'results','SFDR', 'Data', sprintf('TestSFDR_CH%d_Bnd%d', Channel, Band(2))));
    
            % Perform an FFT
            FFT = analysisTools.freqAnalysis(data.test_signal, FFTlen, 2); % Averaged FFT of the signal
            % Find artifact peaks to calculate SFDR
            fprintf("\t Starting SFDR calculations ... ");
            coarse = struct('freq',0,'amplitude',0,'sfdr',0);
            fine = struct('freq',0,'amplitude',0,'sfdr',0);
            fundamental = struct('freq', 0, 'amplitude', 0, 'index', 0);
            % Convert SFDR frequency bounds to indexes
            msdFine = BW*FFTlen/self.Fs;
            msdCoarse = 500e6*FFTlen/self.Fs;

            % Find peaks between 200 KHz and 5 MHz around the target frequency
            [pks,pkIdx] = findpeaks(FFT);
            [maxVal,maxIdx] = max(FFT);
            % Max amplitude frequency is the fundamental
            fundamental.amplitude = analysisTools.conversiondBm(maxVal);
            fundamental.index = maxIdx;
            fundamental.freq = maxIdx*self.Fs/FFTlen;
            
            peaksFine = zeros(1,2);
            % Determine the peaks inside the fine bounds
            for i = 1:length(pks)
                if (pks(i) > 0 && (pkIdx(i) > maxIdx + msdFine || pkIdx(i) < maxIdx - msdFine) ...
                       && (pkIdx(i) < maxIdx + msdCoarse && pkIdx(i) > maxIdx - msdCoarse))
                    peaksFine(end+1, 1) = pks(i);
                    peaksFine(end, 2) = pkIdx(i);
                end
            end

            % Calculate the SFDR of the spurs close to the target frequency
            [spurValFine, spurIdxFine] = max(peaksFine(:,1));
            fine.amplitude = analysisTools.conversiondBm(spurValFine);
            fine.index = peaksFine(spurIdxFine, 2);
            fine.freq = fine.index*self.Fs/FFTlen;
            fine.sfdr = analysisTools.conversiondBm(maxVal) - fine.amplitude;

            % Find peaks above 5 MHz around the target frequency
            peaksCoarse = zeros(1,2);
            % Determine the peaks outside the fine bounds
            for i = 1:length(pks)
                if (pks(i) > 0 && (pkIdx(i) > maxIdx + msdCoarse || pkIdx(i) < maxIdx - msdCoarse))
                    peaksCoarse(end+1, 1) = pks(i);
                    peaksCoarse(end, 2) = pkIdx(i);
                end
            end

            % Calculate the SFDR of the spurs far from the target frequency
            [spurValCoarse, spurIdxCoarse] = max(peaksCoarse(:,1));
            coarse.amplitude = analysisTools.conversiondBm(spurValCoarse);
            coarse.index = peaksCoarse(spurIdxCoarse, 2);
            coarse.freq = coarse.index*self.Fs/FFTlen;
            coarse.sfdr = analysisTools.conversiondBm(maxVal) - coarse.amplitude;
            disp("Finished");
            fprintf('----------------------------------------------------------------\n');
            
            % Convert FFT to dBm
            FFT = analysisTools.conversiondBm(FFT);
            
            save(fullfile(self.baseDir, 'results', 'SFDR', 'Analysis', sprintf('TestSFDR_CH%d_Bnd%d_%gMHz', Channel, Band(2), BW/1e6)),...
                'FFT', 'fundamental', 'fine', 'coarse');
            if(self.run_figures)
               self.SFDRFigures(FFTlen, BW, Band, Channel); 
            end
       end
       function SFDRFigures(self, FFTlen, BW, Band, Channel)
           load(fullfile(self.baseDir, 'results', 'SFDR', 'Analysis', sprintf('TestSFDR_CH%d_Bnd%d_%gMHz', Channel, Band(2), BW/1e6)));
           %% Plot
            f = figure();
            f.Position = [100 100 1000 800];

            s = 1;
            e = FFTlen/2;
            freq =(s:e)*self.Fs/FFTlen/1e6;
            subplot(2, 2, 1:2); hold on;
            if(coarse.amplitude >= -120)
                rectangle('Position',[1 coarse.amplitude 500 (fundamental.amplitude-coarse.amplitude)], 'FaceColor', '#FFEFD6', 'LineStyle', 'none');
                plot(coarse.freq/1e6, coarse.amplitude, 'v', 'Color', [0.8500 0.3250 0.0980]);
            end
            plot(fundamental.freq/1e6, fundamental.amplitude, 'v', 'Color', [0.6350 0.0780 0.1840]); hold on
            plot(freq, FFT(s:e), 'Color', 'Blue'); 
            axis([freq(1) freq(end) -120 (max(FFT)+20)])
            title('SFDR')
            xlabel('Frequency (MHz)')
            ylabel('Amplitude (dBm)')
            ratio = max(FFT)+ 80;
            text(fundamental.freq/1e6, fundamental.amplitude+0.05*ratio, "\textbf{Fundamental:}" + string(round(fundamental.amplitude,2)), 'HorizontalAlignment','center', 'Interpreter', 'latex') 
            dim = [0.69, 0.815, 0.6, 0.1];
            str = "SFDR ($<\pm$ 5 MHz): " + string(round(fine.sfdr, 2));
            annotation('textbox', 'String', str, 'Position', dim, 'FontWeight', 'bold', 'FitBoxToText', 'on','FontSize', 12, 'LineStyle', 'none', 'HorizontalAlignment','left', 'Interpreter', 'latex');
            dim = [0.69, 0.79, 0.6, 0.1];
            str = "SFDR ($>\pm$ 5 MHz): " + string(round(coarse.sfdr,2));
            annotation('textbox', 'String', str, 'Position', dim, 'FontWeight', 'bold', 'FitBoxToText', 'on', 'FontSize', 12, 'LineStyle', 'none', 'HorizontalAlignment','left', 'Interpreter', 'latex'); 
            
%             s = int32(FFTlen/2 -(abs(FFTlen/2-fine.index))*1.3); if(s < 0) s = 1; end
            s = floor((49*FFTlen/2)/100);
%             e = int32(FFTlen/2 +(abs(FFTlen/2-fine.index))*1.3); if(e > FFTlen) e = FFTlen; end
            e = ceil((51*FFTlen/2)/100);
            freq =(double(s):double(e))*self.Fs/FFTlen/1e6;
            subplot(2, 2, 3); hold on
            plot(freq, FFT(s:e), 'Color', 'Blue');
            if(fine.amplitude >= -120)
                plot(fine.freq/1e6, fine.amplitude, 'v', 'Color', [0.8500 0.3250 0.0980]);
                axis([freq(1) freq(end) (FFT(s)-5) (fundamental.amplitude + 5)])
            end
            title('Within 10MHz Span')
            xlabel('Frequency (MHz)')
            ylabel('Amplitude (dBm)')
            ratio = fine.amplitude+70;
            if(fine.amplitude >= -120)
                text(fine.freq/1e6, fine.amplitude+0.075*ratio, "\textbf{Spur:} \\" + string(round(fine.amplitude,2)), 'HorizontalAlignment','center', 'Interpreter', 'latex')
            end
            
            s = int32(FFTlen/2 -(abs(FFTlen/2-coarse.index))*1.3); if(s < 0) s = 1; end
            e = int32(FFTlen/2 +(abs(FFTlen/2-coarse.index))*1.3); if(e > FFTlen/2) e = FFTlen/2; end
            freq =(double(s):double(e))*self.Fs/FFTlen/1e6;
            subplot(2,2,4); hold on
            plot(freq, FFT(s:e), 'Color', 'Blue');
            if(coarse.amplitude >= -120)
                plot(coarse.freq/1e6, coarse.amplitude, 'v', 'Color', [0.8500 0.3250 0.0980]);
                axis([freq(1) freq(end) -120 (fundamental.amplitude + 5)])
            end
            title('Outside 10MHz Span')
            xlabel('Frequency (MHz)')
            ylabel('Amplitude (dBm)')
            if(coarse.amplitude >= -120)
                text(coarse.freq/1e6, coarse.amplitude+ 0.075*ratio, "\textbf{Spur:} \\" + string(round(coarse.amplitude,2)), 'HorizontalAlignment','center', 'Interpreter', 'latex')
            end
            savefig(fullfile(self.baseDir, 'results', 'SFDR', 'Figures', sprintf('TestSFDR_CH%d_Bnd%d_%gMHz', Channel, Band(2), BW/1e6)));
            close all;
       end
   end
end