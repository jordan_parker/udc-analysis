# UDC Analysis
[Base RFUDC repository](https://gitlab.com/simbiant/MySkyTech/rf_updown_fpga)
## Current Build Model Features
- Signal Generator
    - Output Frequency: 100 - 400 MHz
    - Output Gain Control: -75 - 3.97dBm
    - Pulsed Control PW and PRI
    - AM capability for linearity testing
    - Outputs on 3 channels DAC0, DAC1, and DAC2
- Mirror Port on DAC3 for ADC0
- LVDS Control
    - Trigger Width (Should always be greater than 3)
    - Individual Trigger delays for Rx and Tx triggers
    - Configurable frequency hopping addresses
    - Static addressing of DDS tables
- DMA Input
    - Input on 3 channels ADC0, ADC1, and ADC2
    - Records max 500 $\mu$s / 2^19 samples
    - Configurable downsample factor for adjustable resolution bandwidth
    - Interleaved with selectable Reference Input
        - Reference Input Signal, or
        - Reference Trigger Signal

## Using the RFUDC Software Tools
The RFUDC can be used with software tools in a variety of different methods: the full automated test software, the individual test functions, the RFUDC software setting, and the RFUDC GUI control.

### Automated testing (automatedTest.m)
To use the automated testing, the RFUDC must be fully connected to the SLRT machine i.e., all 3 channels, the triggers, and LVDS cable. The `gm_Analysis_slrt.slx` must also be built prior to running the automated test. On some SLRTs a `tg.reboot` may be required due to lost FPGA issues. Once built and restarted the automated test script `automatedTest.m` can be run and left to test. <br/>
- Data Collection approx. 1s for each dataset (80s)
- Results Generation approx. 120s for each dataset (9600s / 2h 40m)
- Report Generation approx. 120s in total (dependent on number of datasets)

### Individual tests (analysisTools.m)
Each individual test can be run using the public function wrappers in `analysisTools.m`. Help for how to use each individual function can be found by typing `help (functionName)` into the matlab command. Each function also has a reference documentation page.

### RFUDC Software Setting (RFUDCTools.m)
The RFUDC can be set directly using the software tools in `RFUDCTools.m`. Frequency Hopping can be controlled using the matlab SLRT commands.<br/>

***RFUDCTools***<br/>
- `init(port)` opens the COM Port
- `closePort()` closes the COM port
- `frequency_set(frequency,channel)` sets the channel frequency
- `attenuation_set(attenuation, channel, target)` sets the channel tx or rx attenuation
- `dds_table_set(frequency, index)` sets the DDS frequency in the corrsponding table entry
- `dds_set(frequency)` sets the DDS frequency through serial
- `powerDown(channel)` run for channels that have been turned off. 
<br/>

***Simulink Real Time Parameters***<br/>
Run `tg.setparam('parameterName', parameterValue)` to change SLRT parameters.
- `channel_select` determines the frequency channel i.e., 100 - 400 MHz selectable output.
- `static_idx` updates the DDS frequency with the specified index.
- `vectorised_idx` updates the frequency hopping pattern.
- `switching_period_us` determines the switching period in $\mu$s.
- `trigger_width` is the width of the trigger signal. (Should always be greater than 3).
- `dma_enable` toggles the read at the ADCs
- `board_select` determines which ADC channel is read.
- `gain_control` alters the amplitude of the output signal.

### RFUDC GUI (RFUDCToolsApp.m)
Run `RFUDCToolsApp.m` from the Matlab command line to open a User Interface for interacting with the RFUDC.

## Future Improvements
- Automated Test script needs a function wrapper or GUI for ease of control over automated test inputs and outputs.
- The Rx and Tx trigger are currently identical but for delayed switching it is important to discern which corresponds to which configuration.
- Settling time has some weird outstanding cases still.
- AM is implemented on the model but not utilised by the tests.
- User help can be collated and inserted into a user manual doc.
- Add SLRT commands to the GUI and RFUDCTools class.
